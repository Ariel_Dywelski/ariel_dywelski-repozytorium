package com.example.uzytkownik.sharedpreferences_zad1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.uzytkownik.sharedpreferences_zad1.language.Country;
import com.example.uzytkownik.sharedpreferences_zad1.language.Language;

import java.util.ArrayList;
import java.util.List;

import static com.example.uzytkownik.sharedpreferences_zad1.MainActivity.COUNTRY_KEY;
import static com.example.uzytkownik.sharedpreferences_zad1.MainActivity.LANGUAGE_KEY;
import static com.example.uzytkownik.sharedpreferences_zad1.MainActivity.NAME_KEY;
import static com.example.uzytkownik.sharedpreferences_zad1.MainActivity.SHARED_PREFERENCES_FILE;
import static com.example.uzytkownik.sharedpreferences_zad1.MainActivity.SURNAME_KEY;


public class FormActivity extends AppCompatActivity {

    private EditText name_input;
    private EditText surname_input;

    private Spinner languageSpinner;
    private Spinner countrySpinner;

    private Button saveButton, cancelButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        name_input = (EditText) findViewById(R.id.nameEditText);
        surname_input = (EditText) findViewById(R.id.surnameEditText);

        saveButton = (Button) findViewById(R.id.saveButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);

        languageSpinner = (Spinner) findViewById(R.id.spinnerLanguage);

        List<Language> spinnerArray = new ArrayList<Language>();
        spinnerArray.add(Language.POLISH);
        spinnerArray.add(Language.ENGLISH);
        spinnerArray.add(Language.GERMAN);
        spinnerArray.add(Language.SPANISH);
        spinnerArray.add(Language.VIETNAMESE);


        ArrayAdapter<Language> stringArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);

        languageSpinner.setAdapter(stringArrayAdapter);

        countrySpinner = (Spinner) findViewById(R.id.spinnerCountry);

        List<Country> countryArray = new ArrayList<>();
        countryArray.add(Country.POLAND);
        countryArray.add(Country.UNITED_KINGDOM);
        countryArray.add(Country.GERMANY);
        countryArray.add(Country.SPAIN);
        countryArray.add(Country.VIETNAM);

        ArrayAdapter<Country> countryAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, countryArray);

        countrySpinner.setAdapter(countryAdapter);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnToMainActivity(RESULT_CANCELED);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePreferences();
                returnToMainActivity(RESULT_OK);

            }
        });

        checkIfEmptyFields();

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkIfEmptyFields();
            }
        };

        View.OnKeyListener listener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                checkIfEmptyFields();
                return false;
            }
        };

        name_input.addTextChangedListener(watcher);
        surname_input.addTextChangedListener(watcher);

    }

    private void checkIfEmptyFields() {
        if (name_input.getText().toString().isEmpty() && surname_input.getText().toString().isEmpty()) {
            saveButton.setEnabled(false);
            cancelButton.setEnabled(false);
        } else {
            saveButton.setEnabled(true);
            cancelButton.setEnabled(true);
        }
    }

    private void savePreferences() {
        SharedPreferences preferences = getSharedPreferences(SHARED_PREFERENCES_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(NAME_KEY, name_input.getText().toString());
        editor.putString(SURNAME_KEY, surname_input.getText().toString());
        editor.putString(LANGUAGE_KEY, languageSpinner.getSelectedItem().toString());
        editor.putString(COUNTRY_KEY, countrySpinner.getSelectedItem().toString());
        editor.commit();
    }


    private void returnToMainActivity(int result) {
        Intent returnIntent = new Intent();
        setResult(result, returnIntent);
        finish();

    }

}
