package com.example.uzytkownik.sharedpreferences_zad1.language;

public enum Country {

    POLAND,
    UNITED_KINGDOM,
    GERMANY,
    SPAIN,
    VIETNAM

}
