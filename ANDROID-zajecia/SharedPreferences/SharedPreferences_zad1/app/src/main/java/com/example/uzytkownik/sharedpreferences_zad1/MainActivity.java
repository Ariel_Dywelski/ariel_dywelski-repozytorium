package com.example.uzytkownik.sharedpreferences_zad1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String SHARED_PREFERENCES_FILE = "textPreferences.txt";
    public static final String NAME_KEY = "name";
    public static final String SURNAME_KEY = "surname";
    public static final String LANGUAGE_KEY = "language";
    public static final String COUNTRY_KEY = "country";


    private TextView name;
    private TextView surname;
    private TextView language;
    private TextView country;
    private Button editButton;
    private String nameToReturn, surnameToReturn;
    private String countryToReturn, languageToReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView) findViewById(R.id.nameText);
        surname = (TextView) findViewById(R.id.surnameText);
        language = (TextView) findViewById(R.id.languageText);
        country = (TextView) findViewById(R.id.countryText);
        editButton = (Button) findViewById(R.id.editTextButton);
        language = (TextView) findViewById(R.id.languageText);
        country = (TextView) findViewById(R.id.countryText);


        loadDataFromForm();


        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSecondActivity();
            }
        });

    }

    @Override
    protected void onResume() {
        loadDataFromForm();

        if(nameToReturn.isEmpty() || surnameToReturn.isEmpty()){
            startSecondActivity();
        }

        name.setText(nameToReturn);
        surname.setText(surnameToReturn);
        language.setText(languageToReturn);
        country.setText(countryToReturn);

        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_SHORT).show();

        }else {
            Toast.makeText(getApplicationContext(), "NOT", Toast.LENGTH_SHORT).show();

        }

    }

    private void loadDataFromForm() {

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);

        nameToReturn = sharedPreferences.getString(NAME_KEY, "");
        surnameToReturn = sharedPreferences.getString(SURNAME_KEY, "");
        languageToReturn = sharedPreferences.getString(LANGUAGE_KEY, "");
        countryToReturn = sharedPreferences.getString(COUNTRY_KEY, "");
    }

    private void startSecondActivity() {
        Intent intent = new Intent(getApplicationContext(), FormActivity.class);

        intent.putExtra(NAME_KEY, nameToReturn);
        intent.putExtra(SURNAME_KEY, surnameToReturn);
        intent.putExtra(COUNTRY_KEY, countryToReturn);

        startActivityForResult(intent, 1);
    }
}
