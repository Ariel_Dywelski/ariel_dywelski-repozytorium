package com.example.uzytkownik.sharedpreferences_zad1.language;

public enum Language {

    POLISH,
    ENGLISH,
    GERMAN,
    SPANISH,
    VIETNAMESE
}
