package oo.max.servicebindexample;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class BindExampleService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        log("service create");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyCustomBinder();
    }

    public class MyCustomBinder extends Binder {
        public BindExampleService getService() {
            return BindExampleService.this;
        }
    }

    public String getHello() {
        return "Hello world from service!";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        log("service destroy");
    }

    private void log(String msg) {
        Log.e("BindExampleService", msg);
    }

}
