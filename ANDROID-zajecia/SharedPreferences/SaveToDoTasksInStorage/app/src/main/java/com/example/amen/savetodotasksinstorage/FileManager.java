package com.example.amen.savetodotasksinstorage;

import android.content.Context;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by amen on 1/16/17.
 */

public class FileManager {
    private static final String FILE_NAME = "todotasks.txt";

    public static final FileManager instance = new FileManager();

    private FileManager() {
    }

    public void saveToFile(Context context, List<ToDoTask> listToSave) {
        try {
            FileOutputStream ofs = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            OutputStreamWriter writer = new OutputStreamWriter(ofs);
            PrintWriter printWriter = new PrintWriter(writer);

            for (ToDoTask task : listToSave) {
                printWriter.println(task.toSerializedString());
            }

            printWriter.close();
        } catch (FileNotFoundException fnfe) {
            Toast.makeText(context, "File not A found.", Toast.LENGTH_SHORT).show();
        } catch (IOException ioe) {
            Toast.makeText(context, "File not W found.", Toast.LENGTH_SHORT).show();
        }
    }

    public List<ToDoTask> getFromFile(Context context) {
        List<ToDoTask> returnList = new LinkedList<>();

        try {
            FileInputStream inputStreamReader = context.openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStreamReader));

//            InputStreamReader inputStreamReader = new InputStreamReader(context.openFileInput(FILE_NAME));
//            BufferedReader reader = new BufferedReader(inputStreamReader);

            String line = reader.readLine();

            while (line != null && !line.isEmpty()) {
                returnList.add(new ToDoTask(line));

                line = reader.readLine();
            }

            reader.close();
        } catch (FileNotFoundException fnfe) {
            Toast.makeText(context, "File not S found.", Toast.LENGTH_SHORT).show();
        } catch (IOException ioe) {
            Toast.makeText(context, "Read exception.", Toast.LENGTH_SHORT).show();
        }

        return returnList;
    }
}
