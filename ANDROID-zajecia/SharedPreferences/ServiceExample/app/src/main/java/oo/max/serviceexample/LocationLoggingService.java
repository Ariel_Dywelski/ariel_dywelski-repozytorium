package oo.max.serviceexample;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class LocationLoggingService extends Service {

    private LocationListener listener;

    @Override
    public void onCreate() {
        super.onCreate();

        requestLocationUpdates();
    }

    private void requestLocationUpdates() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        try {
            listener = buildListener();
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, listener);
        } catch (SecurityException e) {
            Log.e("LocationService", e.getMessage());
        }
    }

    private LocationListener buildListener() {
        return new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                Log.e("LocationService", "location: " + location.getLatitude() + " / " + location.getLongitude());
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {
            locationManager.removeUpdates(listener);
        } catch (SecurityException e) {
            Log.e("LocationService", e.getMessage());
        }

        super.onDestroy();
    }

}
