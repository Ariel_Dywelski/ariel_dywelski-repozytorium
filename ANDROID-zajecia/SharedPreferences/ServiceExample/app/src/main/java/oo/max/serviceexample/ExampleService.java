package oo.max.serviceexample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExampleService extends Service {

    private int key;

    private ScheduledExecutorService executorService;

    @Override
    public void onCreate() {
        super.onCreate();
        log("service create");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            key = intent.getIntExtra("key", -1);
            log("service start command: " + key);
            restartLogging();
        } else {
            stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void restartLogging() {
        stopExecutor();
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                log("my key: " + key);
            }
        }, 1, 1, TimeUnit.SECONDS);
    }

    private void stopExecutor() {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        stopExecutor();
        super.onDestroy();
        log("service destroy");
    }

    private void log(String msg) {
        Log.e("ExampleService", msg);
    }

}
