package oo.max.serviceexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        log("create");

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ExampleService.class);
                intent.putExtra("key", new Random().nextInt(100));
                startService(intent);
            }
        });

        findViewById(R.id.stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ExampleService.class);
                stopService(intent);
            }
        });

        startService(new Intent(this, LocationLoggingService.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        log("start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        log("resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        log("pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        log("stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        log("destroy");
        stopService(new Intent(this, LocationLoggingService.class));
    }

    private void log(String msg) {
        Log.e("MainActivity", msg);
    }
}
