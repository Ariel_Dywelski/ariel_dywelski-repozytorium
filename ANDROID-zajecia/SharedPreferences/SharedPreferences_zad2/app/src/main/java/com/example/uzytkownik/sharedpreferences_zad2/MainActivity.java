package com.example.uzytkownik.sharedpreferences_zad2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public static final String PREFERENCES_NAME = "view.preferences";
    public static final String KEY_WIDTH = "view.width";
    public static final String KEY_HEIGHT = "view.height";
    public static final String KEY_FONT_SIZE = "view.font_size";
    public static final String KEY_BTN_COLOR = "view.btn_color";


    private Button firstButton;
    private Button secondButton;
    private Button thirdButton;
    private Button fourthButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstButton = (Button) findViewById(R.id.buttonFirst);
        secondButton = (Button) findViewById(R.id.buttonSecond);
        thirdButton = (Button) findViewById(R.id.buttonThird);
        fourthButton = (Button) findViewById(R.id.buttonFourth);


    }

    private void setButtonStyle(Button btnToSet, int width, int height, int fontSize, int color){
        btnToSet.setWidth(width);
        btnToSet.setHeight(height);
        btnToSet.setTextSize((float) fontSize);
        btnToSet.setBackgroundColor(color);

    }

    private int parseColor(String color){
        switch (color){
            case "Red":
                return Color.RED;
            case "Green":
                return Color.GREEN;
            case "Blue":
                return Color.BLUE;
            case "Black":
                return Color.BLACK;
            case "White":
                return Color.WHITE;
            case "Orange":
                return Color.YELLOW;
            default:
                return Color.CYAN;
        }
    }

    @Override
    protected void onResume() {
        loadButtonPreferences();
        super.onResume();
    }

    private void loadButtonPreferences() {

        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        int widthToReturn, heightToReturn, fontSizeToReturn, colorInt;
        String colorToReturn;

        widthToReturn = sharedPreferences.getInt(KEY_WIDTH, -1);
        heightToReturn = sharedPreferences.getInt(KEY_HEIGHT, -1);
        fontSizeToReturn = sharedPreferences.getInt(KEY_FONT_SIZE, -1);
        colorToReturn = sharedPreferences.getString(KEY_BTN_COLOR, "");

        colorInt = parseColor(colorToReturn);

        if(widthToReturn == -1 || heightToReturn == -1){
            return;
        }

        setButtonStyle(firstButton, widthToReturn, heightToReturn, fontSizeToReturn, colorInt);
        setButtonStyle(secondButton, widthToReturn, heightToReturn, fontSizeToReturn, colorInt);
        setButtonStyle(thirdButton, widthToReturn, heightToReturn, fontSizeToReturn, colorInt);
        setButtonStyle(fourthButton, widthToReturn, heightToReturn, fontSizeToReturn, colorInt);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_clear){
            clearPreferences();
            loadButtonPreferences();
            return true;

        }else if(id == R.id.action_edit){

            openPreferencesEditor();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void openPreferencesEditor() {
        Intent intent = new Intent(this, EditActivity.class);
        startActivityForResult(intent, 1);
    }

    private void clearPreferences() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.clear();
        editor.commit();

    }
}
