package com.example.uzytkownik.sharedpreferences_zad2;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import static com.example.uzytkownik.sharedpreferences_zad2.MainActivity.KEY_BTN_COLOR;
import static com.example.uzytkownik.sharedpreferences_zad2.MainActivity.KEY_FONT_SIZE;
import static com.example.uzytkownik.sharedpreferences_zad2.MainActivity.KEY_HEIGHT;
import static com.example.uzytkownik.sharedpreferences_zad2.MainActivity.KEY_WIDTH;
import static com.example.uzytkownik.sharedpreferences_zad2.MainActivity.PREFERENCES_NAME;

public class EditActivity extends AppCompatActivity {

    private EditText inputWidth, inputHeight;
    private Spinner colorsSpinner;
    private SeekBar seekBarFont;
    private Button saveButton;

    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        inputHeight = (EditText) findViewById(R.id.inputHeight);
        inputWidth = (EditText) findViewById(R.id.inputWidth);
        colorsSpinner = (Spinner) findViewById(R.id.colors_spinner);
        saveButton = (Button) findViewById(R.id.save_button);

        seekBarFont = (SeekBar) findViewById(R.id.seekBarFont);

        adapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.colors));
        colorsSpinner.setAdapter(adapter);


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePreference();

                setResult(RESULT_OK);
                finish();
            }
        });

        loadPreferences();
    }

    private void loadPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);

        int widthToReturn, heightToReturn, fontSizeToReturn;
        String colorToReturn;

        widthToReturn = sharedPreferences.getInt(KEY_WIDTH, -1);
        heightToReturn = sharedPreferences.getInt(KEY_HEIGHT, -1);
        fontSizeToReturn = sharedPreferences.getInt(KEY_FONT_SIZE, -1);
        colorToReturn = sharedPreferences.getString(KEY_BTN_COLOR, "");

        inputHeight.setText(String .valueOf(widthToReturn));
        inputWidth.setText(String.valueOf(heightToReturn));

        seekBarFont.setProgress(fontSizeToReturn);

        int zaznaczonyColor = adapter.getPosition(colorToReturn);

        colorsSpinner.setSelection(zaznaczonyColor);
    }

    private void savePreference() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        String widthString = inputWidth.getText().toString();
        String heightString = inputHeight.getText().toString();

        int wightInteger = -1;
        int heightInteger = -1;
        try{
            if(!widthString.isEmpty()){
                wightInteger = Integer.parseInt(widthString);
            }
            if(!heightString.isEmpty()){
                heightInteger = Integer.parseInt(heightString);
            }

        }catch (NumberFormatException nfe){
            Toast.makeText(getApplicationContext(), "Wrong data", Toast.LENGTH_SHORT).show();
            return;
        }
        editor.putInt(KEY_WIDTH, wightInteger);
        editor.putInt(KEY_HEIGHT, heightInteger);
        editor.putInt(KEY_FONT_SIZE, seekBarFont.getProgress());
        editor.putString(KEY_BTN_COLOR, colorsSpinner.getSelectedItem().toString());

        editor.apply();

    }


}
