package com.example.uzytkownik.sensorapplication;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SensorActivity extends AppCompatActivity implements SensorEventListener{


    private TextView axis_x_label;
    private TextView axis_y_label;
    private TextView axis_z_label;
    private Button saveStopButton;


    private SensorManager sensorManager;
    private Sensor accelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        axis_x_label = (TextView) findViewById(R.id.axis_x);
        axis_y_label = (TextView) findViewById(R.id.axis_y);
        axis_z_label = (TextView) findViewById(R.id.axis_z);
        saveStopButton = (Button) findViewById(R.id.stopButton);

        SensorFileManager.instance.openFile(getApplicationContext());

        saveStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SensorFileManager.instance.closeWriter();
                finish();

                Intent stopIntent = new Intent(SensorActivity.this, FirstActivity.class);
                startActivity(stopIntent);

            }
        });

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }


    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        float x = sensorEvent.values[0];
        String string_X = String.valueOf(x);
        axis_x_label.setText(string_X);


        float y = sensorEvent.values[1];
        String string_Y = String.valueOf(y);
        axis_y_label.setText(string_Y);

        float z = sensorEvent.values[2];
        String string_Z = String.valueOf(z);
        axis_z_label.setText(string_Z);

        SensorFileManager.instance.saveToFile(x,y,z);


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();

        sensorManager.unregisterListener(this,accelerometer);

    }
}
