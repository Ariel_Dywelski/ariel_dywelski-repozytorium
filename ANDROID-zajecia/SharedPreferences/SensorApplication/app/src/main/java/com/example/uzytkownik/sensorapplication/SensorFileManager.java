package com.example.uzytkownik.sensorapplication;

import android.content.Context;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class SensorFileManager {

    public static final String FILE_NAME = "sensorresult";

    public static final SensorFileManager instance = new SensorFileManager();

    private FileOutputStream fileOutputStream;
    private PrintWriter printWriter;

    public void openFile(Context context) {
        try {
            fileOutputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            printWriter = new PrintWriter(new OutputStreamWriter(fileOutputStream));

        } catch (FileNotFoundException e) {
            Toast.makeText(context.getApplicationContext(), "File not found", Toast.LENGTH_SHORT).show();
        }

    }

    public void saveToFile (float x, float y, float z){
        printWriter.print(x);
        printWriter.print(";");
        printWriter.print(y);
        printWriter.print(";");
        printWriter.println(z);
    }

    public void closeWriter(){
        printWriter.close();
    }

}
