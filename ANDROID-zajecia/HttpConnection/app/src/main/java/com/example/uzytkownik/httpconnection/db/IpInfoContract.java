package com.example.uzytkownik.httpconnection.db;

import android.provider.BaseColumns;

public class IpInfoContract  {

    public static class IpInfoEntry implements BaseColumns {

        public static final String TABEL_NAME = "ip_info";
        public static final String CITY_COLUMN_NAME = "city";
        public static final String COUNTRY_COLUMN_NAME = "country";


    }

}
