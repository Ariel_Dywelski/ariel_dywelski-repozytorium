package com.example.uzytkownik.httpconnection.model;

public class Result {

    private String countryIdo2;
    private String stateAbbr;
    private String postal;
    private String continent;
    private String state;
    private String longitude;
    private String latitude;
    private String ds;
    private String network;
    private String city;
    private String country;
    private String ip;

    public String getCountryIdo2() {
        return countryIdo2;
    }

    public String getStateAbbr() {
        return stateAbbr;
    }

    public String getPostal() {
        return postal;
    }

    public String getContinent() {
        return continent;
    }

    public String getState() {
        return state;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getDs() {
        return ds;
    }

    public String getNetwork() {
        return network;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getIp() {
        return ip;
    }

    public void setCountryIdo2(String countryIdo2) {
        this.countryIdo2 = countryIdo2;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setDs(String ds) {
        this.ds = ds;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
