package com.example.uzytkownik.httpconnection;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.example.uzytkownik.httpconnection.db.IpInfoContract;
import com.example.uzytkownik.httpconnection.db.IpInfoDBHelper;

import java.util.ArrayList;
import java.util.List;

public class ReadDataBaseAsyncTask extends AsyncTask<Void, Void, List<String>> {

    private final Context context;
    private final SQLiteDatabase readData;
    private final GetIpInfoListener getIpInfoListener;

    public ReadDataBaseAsyncTask(Context context, GetIpInfoListener getIpInfoListener) {
        this.context = context;
        this.getIpInfoListener = getIpInfoListener;
        IpInfoDBHelper dbHelper = new IpInfoDBHelper(context);
        readData = dbHelper.getReadableDatabase();

    }

    @Override
    protected List<String> doInBackground(Void... voids) {
        Cursor ipInfos = readData.query(IpInfoContract.IpInfoEntry.TABEL_NAME, null, null, null, null, null, null);
        List<String> results = new ArrayList<>();

        while (ipInfos.moveToNext()) {
            String cityName = ipInfos.getString(1);
            String countryName = ipInfos.getString(2);

            results.add(cityName + " , " + countryName);

        }

        return results;
    }

    @Override
    protected void onPostExecute(List<String> results) {
        getIpInfoListener.onInfoGet(results);
    }

    public interface GetIpInfoListener {
        void onInfoGet(List<String> results);
    }
}
