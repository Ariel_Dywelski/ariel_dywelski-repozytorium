package com.example.uzytkownik.httpconnection.model;

import com.google.gson.annotations.SerializedName;

public class IpInfo {

    @SerializedName("RestResponse")
    private RestResponse restResponse;


    public RestResponse getRestResponse() {
        return restResponse;
    }

    public void setRestResponse(RestResponse restResponse) {
        this.restResponse = restResponse;
    }
}
