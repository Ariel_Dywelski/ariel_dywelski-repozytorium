package com.example.uzytkownik.httpconnection;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.uzytkownik.httpconnection.model.IpInfo;

import java.util.List;

public class MainActivity extends AppCompatActivity implements GetIPInfoAsyncTask.GetIpInfoListener, ReadDataBaseAsyncTask.GetIpInfoListener {


    private EditText editTextIp;
    private Button buttonIpInfo;
    private TextView textView;
    private TextView textView1;
    private ListView dataBaseListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataBaseListView = (ListView) findViewById(R.id.dataBaseListView);
        editTextIp = (EditText) findViewById(R.id.editTextIP);
        buttonIpInfo = (Button) findViewById(R.id.buttonGetIPInfo);
        textView = (TextView) findViewById(R.id.cityTextView);
        textView1 = (TextView) findViewById(R.id.countryTextView);

        buttonIpInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ipText = editTextIp.getText().toString();

                new GetIPInfoAsyncTask(MainActivity.this, MainActivity.this).execute(ipText);
            }
        });


        refreshIpInfoList();
    }


    @Override
    public void onGetInfoDownloaded(IpInfo ipInfo) {
        String city = ipInfo.getRestResponse().getResult().getCity();
        textView.setText(city);
        String country = ipInfo.getRestResponse().getResult().getCountry();
        textView1.setText(country);

        refreshIpInfoList();

    }

    private void refreshIpInfoList() {
        new ReadDataBaseAsyncTask(this, this).execute();
    }

    @Override
    public void onInfoGet(List<String> results) {
        ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, results);

        dataBaseListView.setAdapter(adapter);
    }
}
