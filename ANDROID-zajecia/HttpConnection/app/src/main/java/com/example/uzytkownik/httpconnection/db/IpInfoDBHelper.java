package com.example.uzytkownik.httpconnection.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.uzytkownik.httpconnection.db.IpInfoContract.IpInfoEntry;


public class IpInfoDBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "ip_data.db";
    private static final int VERSION = 1;
    private static final String CREATE_IP_INFO_SQL_TABLE = "create table "
            + IpInfoEntry.TABEL_NAME + "("
            + IpInfoEntry._ID + " integer primary key autoincrement,"
            + IpInfoEntry.CITY_COLUMN_NAME + " string,"
            + IpInfoEntry.COUNTRY_COLUMN_NAME + " string)";

    private static final String DROP_IP_INFO_SQL_TABLE = " drop table if exists "
            + IpInfoEntry.TABEL_NAME;


    public IpInfoDBHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_IP_INFO_SQL_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_IP_INFO_SQL_TABLE);
        onCreate(sqLiteDatabase);

    }
}
