package com.example.uzytkownik.httpconnection;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.example.uzytkownik.httpconnection.db.IpInfoContract;
import com.example.uzytkownik.httpconnection.db.IpInfoDBHelper;
import com.example.uzytkownik.httpconnection.model.IpInfo;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GetIPInfoAsyncTask extends AsyncTask<String, Void, IpInfo> {

    private final Context context;
    private final SQLiteDatabase dataBase;
    private Gson gson;
    private final GetIpInfoListener getIpInfoListener;


    public GetIPInfoAsyncTask(GetIpInfoListener getIpInfoListener, Context context) {
        this.getIpInfoListener = getIpInfoListener;
        this.context = context;
        gson = new Gson();
        IpInfoDBHelper dbHelper = new IpInfoDBHelper(context);
        dataBase = dbHelper.getWritableDatabase();
    }

    @Override
    protected IpInfo doInBackground(String... params) {
        String ip = params[0];
        String url = getUrlFromIp(ip);

        try {
            String json = getJsonFromUrl(url);
            IpInfo ipInfo = gson.fromJson(json, IpInfo.class);
            saveIpInfoToDB(ipInfo);
            return ipInfo;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void saveIpInfoToDB(IpInfo ipInfo) {
        ContentValues values = new ContentValues();

        values.put(IpInfoContract.IpInfoEntry.CITY_COLUMN_NAME, ipInfo.getRestResponse().getResult().getCity());
        values.put(IpInfoContract.IpInfoEntry.COUNTRY_COLUMN_NAME, ipInfo.getRestResponse().getResult().getCountry());

        dataBase.insert(IpInfoContract.IpInfoEntry.TABEL_NAME, null, values);


    }


    private String getUrlFromIp(String ip) {

        StringBuilder urlStringBuilder = new StringBuilder();
        urlStringBuilder.append("http://geo.groupkt.com/ip/");
        urlStringBuilder.append(ip);
        urlStringBuilder.append("/json");

        return urlStringBuilder.toString();

    }

    private String getJsonFromUrl(String urlText) throws IOException {

        URL url = new URL(urlText);
        HttpURLConnection connection = null;
        InputStream stream = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            stream = connection.getInputStream();
            String jsonText = IOUtils.toString(stream);
            return jsonText;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (stream != null) {
                stream.close();
            }
        }
        return null;
    }


    public interface GetIpInfoListener {
        void onGetInfoDownloaded(IpInfo ipInfo);

    }

    @Override
    protected void onPostExecute(IpInfo ipInfo) {

        getIpInfoListener.onGetInfoDownloaded(ipInfo);

    }
}
