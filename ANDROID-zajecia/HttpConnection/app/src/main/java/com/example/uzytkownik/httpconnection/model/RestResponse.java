package com.example.uzytkownik.httpconnection.model;

public class RestResponse {

    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
