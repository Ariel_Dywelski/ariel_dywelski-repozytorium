package com.example.uzytkownik.listanddetail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder> {

    private Context context;
    private List<Product> products;
    private final LayoutInflater layoutInflater;
    private final OnProductClickListener onProductClickListener;

    public ProductListAdapter(Context context, List<Product> products, OnProductClickListener onProductClickListener) {
        this.context = context;
        this.products = products;
        layoutInflater = LayoutInflater.from(context);
        this.onProductClickListener = onProductClickListener;
    }

    @Override
    public ProductListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.view_item_product, parent, false);

        return new ProductListViewHolder(view,onProductClickListener);
    }

    @Override
    public void onBindViewHolder(ProductListViewHolder holder, int position) {
        Product product = products.get(position);
        holder.productsName.setText(product.getName());
        holder.product = product;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ProductListViewHolder extends RecyclerView.ViewHolder{
        private TextView productsName;
        private Product product;

        public ProductListViewHolder(View itemView, final OnProductClickListener onProductClickListener) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onProductClickListener.onProductClick(product);
                }
            });
            productsName = (TextView) itemView.findViewById(R.id.itemText);
        }
    }
    public interface OnProductClickListener {
        void onProductClick(Product product);
    }
}
