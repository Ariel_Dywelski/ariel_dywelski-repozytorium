package com.example.uzytkownik.listanddetail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

public class ProductListActivity extends AppCompatActivity implements ProductListAdapter.OnProductClickListener{

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        reloadProducts();



    }

    private void reloadProducts() {
        ProductListAdapter productListAdapter =
                new ProductListAdapter(this, ProductRepository.getInstance().getProducts(), this);


        recyclerView.setAdapter(productListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1 && resultCode == RESULT_OK && data!=null){
            if (data.getBooleanExtra("refresh",false)){
                reloadProducts();

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onProductClick(Product product) {
        Toast.makeText(this, "Product ID: "+product.getId(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("ID", product.getId());
        startActivityForResult(intent, 1);
    }
}
