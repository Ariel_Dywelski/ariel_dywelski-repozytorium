package com.example.uzytkownik.listanddetail;

import java.util.Arrays;
import java.util.List;

public class ProductRepository {

    private List<Product> products = Arrays.asList(
            new Product(1, "Banan"),
            new Product(2, "Pomarańcza"),
            new Product(3, "Jabłko"),
            new Product(4, "Płyn do naczyń")
    );

    private static ProductRepository productRepository;
    public static ProductRepository getInstance(){
        if(productRepository == null){
            productRepository = new ProductRepository();
        }
        return productRepository;
    }

    public Product getProductById(int id) {

        for (Product product : products) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    public List<Product> getProducts() {
        return products;
    }
    public void updateProduct(int id, String name){
        getProductById(id).setName(name);
    }
}
