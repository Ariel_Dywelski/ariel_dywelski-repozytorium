package com.example.uzytkownik.listanddetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DetailsActivity extends AppCompatActivity {

    private EditText productName;
    private Button saveButton;
    private Button cancelButton;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        productName = (EditText) findViewById(R.id.productNameText);
        saveButton = (Button) findViewById(R.id.saveButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductRepository.getInstance().updateProduct(id,productName.getText().toString());
                Intent backIntent = new Intent();
                backIntent.putExtra("refresh",true);
                setResult(RESULT_OK, backIntent);
                finish();
            }
        });

        id = getIntent().getIntExtra("ID", -1);
        if(id == -1){
            finish();
            return;
        }

        Product productById = ProductRepository.getInstance().getProductById(id);
        if(productById == null){
            finish();
            return;
        }
        productName.setText(productById.getName());
    }
}
