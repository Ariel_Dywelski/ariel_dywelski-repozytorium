package com.example.uzytkownik.httpexample;

import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.uzytkownik.httpexample.model.Book;

import java.io.IOException;
import java.util.List;

/**
 * Created by ARIEL on 2016-12-12.
 */

public class TestAsyncTask extends AsyncTask<Integer, Integer, List<Book>> {

    private final TextView textView;
    private final ProgressBar progressBar;
    private final ServerAppClient serverAppClient;

    public TestAsyncTask(TextView textView, ProgressBar progressBar) {
        this.textView = textView;
        this.progressBar = progressBar;
        serverAppClient = new ServerAppClient();
    }

    @Override
    protected void onPreExecute() {
        textView.setText("Start :D");
    }


    @Override
    protected List<Book> doInBackground(Integer... integers) {
        try {
            return serverAppClient.callServerApi();


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Integer currentProgress = values[0];
        textView.setText(String.valueOf(currentProgress));
        progressBar.setProgress(currentProgress);
    }

    @Override
    protected void onPostExecute(List<Book> books) {
        if(books == null){
            textView.setText("Failed");
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for(Book book: books){
            stringBuilder.append(book.getName())
                    .append("\n");
        }
        String result = stringBuilder.toString();
        textView.setText(result);
    }
}
