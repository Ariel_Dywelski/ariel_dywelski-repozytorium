package com.example.uzytkownik.httpexample.retrofit;

import com.example.uzytkownik.httpexample.model.BookResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface RetrofitApiClient {

    @GET("/plugin/test.getTestCollection")
    @Headers("X-BAASBOX-APPCODE: 1234567890")
    Call<BookResponse> getBooks();
}
