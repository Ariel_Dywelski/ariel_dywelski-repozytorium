package com.example.uzytkownik.httpexample;

/**
 * Created by ARIEL on 2016-12-12.
 */

public class OperationCompletedEvent {

    private final String result;

    public OperationCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}
