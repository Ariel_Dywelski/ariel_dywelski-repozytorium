package com.example.uzytkownik.httpexample;

import android.app.IntentService;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by ARIEL on 2016-12-12.
 */

 public class TestIntentService extends IntentService {

    public TestIntentService() {
        super("TestIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            ServerAppClient serverAppClient = new ServerAppClient();
            //String result = serverAppClient.callServerApi();

           // postResult(result);

        }catch (Exception e){
            e.printStackTrace();
            postResult(null);
        }
    }

    private void postResult(String result){
        OperationCompletedEvent event = new OperationCompletedEvent(result);

        EventBus.getDefault().post(event);
    }
}
