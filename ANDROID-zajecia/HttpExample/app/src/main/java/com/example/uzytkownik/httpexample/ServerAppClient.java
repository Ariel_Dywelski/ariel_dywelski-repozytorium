package com.example.uzytkownik.httpexample;

import com.example.uzytkownik.httpexample.model.Book;
import com.example.uzytkownik.httpexample.model.BookResponse;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by ARIEL on 2016-12-12.
 */

public class ServerAppClient {
    private final Gson gson;

    public ServerAppClient() {
        gson = new Gson();
    }

    public List<Book> callServerApi() throws IOException {
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;

        try {
            URL url = new URL("http://91.134.143.223:9000/asset/test.json?X-BAASBOX-APPCODE=1234567890");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            inputStream = urlConnection.getInputStream();

            String responseBody = IOUtils.toString(inputStream);

            BookResponse bookResponse = gson.fromJson(responseBody, BookResponse.class);
            return bookResponse.getData();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }
}
