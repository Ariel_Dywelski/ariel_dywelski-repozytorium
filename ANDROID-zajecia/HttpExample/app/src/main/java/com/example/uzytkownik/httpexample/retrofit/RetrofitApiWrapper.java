package com.example.uzytkownik.httpexample.retrofit;

import com.example.uzytkownik.httpexample.model.Book;
import com.example.uzytkownik.httpexample.model.BookResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class RetrofitApiWrapper {

    private final RetrofitApiClient retrofitApiClient;

    public RetrofitApiWrapper() {

        ApiClientFactory apiClientFactory = new ApiClientFactory();

        retrofitApiClient = apiClientFactory.createApiClient();

    }

    public List<Book> getBooks() throws IOException {

        Call<BookResponse> call = retrofitApiClient.getBooks();

        Response<BookResponse> response = call.execute();

        BookResponse bookResponse = response.body();

        return bookResponse.getData();

    }
}
