package com.example.uzytkownik.httpexample.model;

import java.util.List;

/**
 * Created by ARIEL on 2016-12-13.
 */

public class BookResponse {

    private final String result;
    private final List<Book> data;
    private final int http_code;

    public BookResponse(String result, List<Book> data, int http_code) {

        this.result = result;
        this.data = data;
        this.http_code = http_code;
    }


    public int getHttp_code() {
        return http_code;
    }

    public List<Book> getData() {
        return data;
    }

    public String getResult() {
        return result;
    }

}
