package com.example.uzytkownik.httpexample.model;

/**
 * Created by ARIEL on 2016-12-13.
 */

public class Book {

    private final String name;

    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
