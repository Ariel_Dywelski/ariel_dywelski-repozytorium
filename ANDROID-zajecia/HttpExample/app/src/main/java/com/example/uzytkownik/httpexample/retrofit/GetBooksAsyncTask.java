package com.example.uzytkownik.httpexample.retrofit;

import android.os.AsyncTask;

import com.example.uzytkownik.httpexample.model.Book;

import java.util.List;

/**
 * Created by ARIEL on 2016-12-13.
 */

public class GetBooksAsyncTask extends AsyncTask<Void, Void, List<Book>> {

    private final RetrofitApiWrapper retrofitApiWraper;
    private final BooksDownloadedListener booksDownloadedListener;


    public GetBooksAsyncTask(BooksDownloadedListener booksDownloadedListener) {
        this.booksDownloadedListener = booksDownloadedListener;
        retrofitApiWraper = new RetrofitApiWrapper();

    }

    @Override
    protected List<Book> doInBackground(Void... voids) {
        try {
            return retrofitApiWraper.getBooks();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Book> books) {
        booksDownloadedListener.onBooksDownloaded(books);
    }

    public interface BooksDownloadedListener {
        void onBooksDownloaded(List<Book> books);

    }
}
