package pl.sdacademy.activityshowtime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SdaActivity extends AppCompatActivity {

    private TextView dateTimeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sda);

        dateTimeView = (TextView) findViewById(R.id.date_time);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String formattedDate = getFormattedCurrentDate();
        dateTimeView.setText(formattedDate);
    }

    private String getFormattedCurrentDate() {
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance();
        Calendar calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }


}
