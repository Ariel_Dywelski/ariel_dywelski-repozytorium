package com.example.uzytkownik.areal.todolist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.uzytkownik.areal.R;

public class ToDoListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);
    }
}
