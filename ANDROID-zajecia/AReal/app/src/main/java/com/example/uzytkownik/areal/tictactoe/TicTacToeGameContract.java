package com.example.uzytkownik.areal.tictactoe;

public interface TicTacToeGameContract {

    interface GameView {
        void fillBoardViews(Boolean[] fieldValues);
        void showPlayerRoundMessage(boolean player);
        void showPlayerWinMessage(boolean player);
        void showDrawMessage();

    }

    interface GamePresenter {
        void onBoardFieldClick(int field);
        void onNewGame();

    }

}
