package com.example.uzytkownik.areal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.uzytkownik.areal.model.BaseUserActivity;
import com.example.uzytkownik.areal.registration.CreateUserActivity;
import com.example.uzytkownik.areal.registration.UserAccount;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.user_Text)
    EditText userName;
    @BindView(R.id.user_password)
    EditText userPassword;

    @BindView(R.id.login_button)
    Button loginButton;
    @BindView(R.id.create_button)
    Button createButton;


    @OnClick(R.id.login_button)
    public void startBaseActivityAfterCorrectLogin(View view) {
        UserAccount account = getFormData();

        if (!account.isValidRegister()){
            Toast.makeText(this, "pola sa puste", Toast.LENGTH_LONG).show();
            return;
        }
        Intent loginIntent = new Intent(this, BaseUserActivity.class);
        startActivity(loginIntent);

    }

    private UserAccount getFormData(){
        UserAccount account = new UserAccount();
        account.setFirstName(userName.getText().toString());
        account.setPassword(userPassword.getText().toString());
        return account;
    }

    @OnClick(R.id.create_button)
    public void startCreateUserActivity(View view) {
        Intent createIntent = new Intent(this, CreateUserActivity.class);
        startActivity(createIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }
}
