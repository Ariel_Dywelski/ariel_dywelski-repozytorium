package com.example.uzytkownik.areal.webbrowser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.uzytkownik.areal.R;

public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
    }
}
