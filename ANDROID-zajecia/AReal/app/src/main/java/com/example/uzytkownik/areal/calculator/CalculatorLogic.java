package com.example.uzytkownik.areal.calculator;

public class CalculatorLogic {

    private static String SIGN_ADD = "+";
    private static String SIGN_SUB = "-";
    private static String SIGN_MUL = "*";
    private static String SIGN_DIV = "/";

    private static boolean validationOperation(String line) {
        int foundSign = 0;
        if (line.contains(SIGN_ADD)) {
            foundSign++;
        }
        if (line.contains(SIGN_SUB)) {
            foundSign++;
        }
        if (line.contains(SIGN_MUL)) {
            foundSign++;
        }
        if (line.contains(SIGN_DIV)) {
            foundSign++;
        }
        if (foundSign != 1) {
            return false;
        }
        return true;
    }

    public static double calculateValue (String line) throws Exception {
        if(!validationOperation(line)){
            throw new Exception("Podano nieprawidłowe dane!");
        }
        if (line.contains(SIGN_ADD)){
            return parseAndCountAdd(line);
        }else if (line.contains(SIGN_SUB)){
            return parseAndCountSub(line);
        }else if (line.contains(SIGN_MUL)){
            return parseAndCountMul(line);
        }else {
            return parseAndCountDiv(line);
        }
    }
    private static double parseAndCountAdd(String line) {
        String [] splits = line.split("\\"+SIGN_ADD);

        double firstInputValue = Double.parseDouble(splits[0]);
        double secondInputValue = Double.parseDouble(splits[1]);
        return add(firstInputValue,secondInputValue);

    }
    private static double parseAndCountSub(String line) {
        String [] splits = line.split("\\"+SIGN_SUB);

        double firstInputValue = Double.parseDouble(splits[0]);
        double secondInputValue = Double.parseDouble(splits[1]);
        return subtraction(firstInputValue,secondInputValue);
    }

    private static double parseAndCountMul(String line) {
        String [] splits = line.split("\\"+SIGN_MUL);

        double firstInputValue = Double.parseDouble(splits[0]);
        double secondInputValue = Double.parseDouble(splits[1]);
        return multiply(firstInputValue,secondInputValue);
    }

    private static double parseAndCountDiv(String line) throws Exception {
        String [] splits = line.split("\\"+SIGN_DIV);

        double firstInputValue = Double.parseDouble(splits[0]);
        double secondInputValue = Double.parseDouble(splits[1]);

        if (secondInputValue == 0.0){
            throw new Exception("You can not divide by 0 !!!");
        }
        return divide(firstInputValue,secondInputValue);
    }



    private static double add(double firstInputValue, double secondInputValue) {
        return firstInputValue+secondInputValue;
    }
    private static double subtraction(double firstInputValue, double secondInputValue) {
        return firstInputValue-secondInputValue;
    }
    private static double multiply(double firstInputValue, double seconndInputValue) {
        return firstInputValue*seconndInputValue;
    }
    private static double divide(double firstInputValue, double secondInputValue) {
        return firstInputValue/secondInputValue;
    }

}
