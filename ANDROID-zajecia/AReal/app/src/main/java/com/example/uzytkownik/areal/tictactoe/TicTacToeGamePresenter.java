package com.example.uzytkownik.areal.tictactoe;

public class TicTacToeGamePresenter implements TicTacToeGameContract.GamePresenter {

    private TicTacToeGameContract.GameView gameView;

    private boolean currentplayer;

    private boolean gameFinished;
    private Boolean[] fieldValues;

    private int[][] winStates = {
            {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}
    };

    public TicTacToeGamePresenter(TicTacToeGameContract.GameView gameView) {
        this.gameView = gameView;
        onNewGame();
    }

    @Override
    public void onNewGame() {
        gameFinished = false;
        fieldValues = new Boolean[9];
        currentplayer = false;
        gameView.fillBoardViews(fieldValues);
        gameView.showPlayerRoundMessage(currentplayer);

    }

    @Override
    public void onBoardFieldClick(int field) {
        if (fieldValues[field] != null || gameFinished) {
            return;
        }

        fieldValues[field] = currentplayer;
        gameView.fillBoardViews(fieldValues);

        if (getWinner() != null){
            gameView.showPlayerWinMessage(currentplayer);
            gameFinished = true;
        }else if (allFieldIsFilled()){
            gameView.showDrawMessage();
        }else {
            currentplayer = !currentplayer;
            gameView.showPlayerRoundMessage(currentplayer);
        }
    }


    private Boolean getWinner() {
        for (int[] win : winStates) {
            if (fieldValues[win[0]] == null || fieldValues[win[1]] == null || fieldValues[win[2]] == null) {
                continue;
            }
            if (fieldValues[win[0]].equals(fieldValues[win[1]]) && fieldValues[win[0]].equals(fieldValues[win[2]])) {
                return fieldValues[win[0]];
            }
        }
        return null;
    }

    private boolean allFieldIsFilled() {
        for (Boolean b : fieldValues) {
            if (b == null) {
                return false;
            }
        }
        return true;
    }
}
