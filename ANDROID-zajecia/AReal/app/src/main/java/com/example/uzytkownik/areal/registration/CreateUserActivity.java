package com.example.uzytkownik.areal.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.uzytkownik.areal.MainActivity;
import com.example.uzytkownik.areal.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateUserActivity extends AppCompatActivity {

    public static final String EXTRA_ACCOUNT = "EXTRA_ACCOUNT";

    @BindView(R.id.firstName)
    EditText firstName;

    @BindView(R.id.lastName)
    EditText lastName;

    @BindView(R.id.birthDate)
    EditText birthDate;

    @BindView(R.id.genderRadioGroup)
    RadioGroup genderGroup;

    @BindView(R.id.userStreet)
    EditText street;

    @BindView(R.id.houseNumber)
    EditText streetNumber;

    @BindView(R.id.userPostalCode)
    EditText postalCode;

    @BindView(R.id.userCity)
    EditText city;

    @BindView(R.id.userCountry)
    EditText country;

    @BindView(R.id.userMailAddress)
    EditText email;

    @BindView(R.id.userPhone)
    EditText phoneNumber;

    @BindView(R.id.userPassword)
    EditText password;

    @BindView(R.id.passwordConfirm)
    EditText passwordConfirm;

    @BindView(R.id.newsletterCheck)
    CheckBox newsletter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.nextButton)
    public void validateFormAndRegisterUser() {
        UserAccount account = getFormData();

        if (!account.isValid()) {
            showToast(R.string.account_error);
            return;
        }

        if (!isPasswordCorrect()) {
            showToast(R.string.password_error);
            return;
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_ACCOUNT, account);
        startActivity(intent);
    }

    private UserAccount getFormData() {
        UserAccount account = new UserAccount();
        account.setFirstName(firstName.getText().toString());
        account.setLastName(lastName.getText().toString());
        account.setBirthDate(birthDate.getText().toString());
        account.setGender(genderGroup.getCheckedRadioButtonId() == R.id.genderRadioGroup ? Gender.MALE : Gender.FEMALE);
        account.setStreet(street.getText().toString());
        account.setStreetNumber(streetNumber.getText().toString());
        account.setPostalCode(postalCode.getText().toString());
        account.setCity(city.getText().toString());
        account.setCountry(country.getText().toString());
        account.setEmail(email.getText().toString());
        account.setPhoneNumber(phoneNumber.getText().toString());
        account.setPassword(password.getText().toString());
        account.setNewsletter(newsletter.isChecked());
        return account;
    }

    private void showToast(int stringResId) {
        Toast.makeText(this, stringResId, Toast.LENGTH_LONG).show();
    }

    private boolean isPasswordCorrect() {
        String password1 = password.getText().toString();
        String password2 = passwordConfirm.getText().toString();
        return password1.equals(password2) && !TextUtils.isEmpty(password1) && password1.length() > 4;
    }
}
