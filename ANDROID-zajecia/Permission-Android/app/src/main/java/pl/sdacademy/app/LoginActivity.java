package pl.sdacademy.app;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    private static final int READ_WRITE_CONTACTS_REQUEST = 123;
    private TemperatureBatteryInfoReceiver temperatureBatteryInfoReceiver;
    private float temp;
    private IntentFilter networkConnectionIntentFilter;
    private BroadcastReceiver networkConnectionReceiver;

    @BindView(R.id.loginButton)
    Button loginButton;

    @BindView(R.id.loginEdit)
    EditText loginEdit;

    @BindView(R.id.passwordEdit)
    EditText passwordEdit;

    @BindView(R.id.permissionMessage)
    TextView messageText;

    @BindView(R.id.noConnectionMessage)
    TextView noConnectionMessageText;

    @BindView(R.id.appLogo)
    ImageView logoImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (doNotHavePermission(Manifest.permission.READ_CONTACTS) || doNotHavePermission(Manifest.permission.WRITE_CONTACTS)) {
            if (shouldShowPermissionRationale(Manifest.permission.READ_CONTACTS) && shouldShowPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                showRationaleDialog();
            } else {
                requestReadWriteContactsPermission();
            }
        } else {
            loginButton.setEnabled(true);
            messageText.setVisibility(View.INVISIBLE);
        }

        Animation rotation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.image_anim);
        logoImageView.startAnimation(rotation);

        networkConnectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (isConnected()) {
                    loginButton.setEnabled(true);
                    noConnectionMessageText.setVisibility(View.GONE);
                } else {
                    loginButton.setEnabled(false);
                    noConnectionMessageText.setVisibility(View.VISIBLE);
                }
            }
        };

        networkConnectionIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        temperatureBatteryInfoReceiver = new TemperatureBatteryInfoReceiver();
        this.registerReceiver(this.temperatureBatteryInfoReceiver, new IntentFilter((Intent.ACTION_BATTERY_CHANGED)));
        temp = temperatureBatteryInfoReceiver.getTemperature();


    }


//    @OnTextChanged({R.id.loginEdit, R.id.passwordEdit})
//    public void isCorrectUser(CharSequence text) {
//        String userLogin = "arieldywelski";
//        String userPassword = "12345";
//
//        if( loginEdit.equals(userLogin) && passwordEdit.equals(userPassword)){
//            loginButton.setEnabled(true);
//        }else{
//            loginButton.setEnabled(false);
//        }
//
//    }


    @OnClick(R.id.loginButton)
    public void login(View view) {
        Intent startMainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(startMainActivityIntent);
    }

    @OnClick(R.id.createButton)
    public void createNewUser(View view) {
        Intent startFormActivity = new Intent(LoginActivity.this, FormActivity.class);
        startActivity(startFormActivity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(networkConnectionReceiver, networkConnectionIntentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(networkConnectionReceiver);
        super.onPause();
    }

    private boolean doNotHavePermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean shouldShowPermissionRationale(String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
    }

    private void requestReadWriteContactsPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS},
                READ_WRITE_CONTACTS_REQUEST);
    }

    private void showRationaleDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestReadWriteContactsPermission();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .create()
                .show();
    }

    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_WRITE_CONTACTS_REQUEST: {
                if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // nadano uprawnienie
                    loginButton.setEnabled(true);
                    messageText.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

}
