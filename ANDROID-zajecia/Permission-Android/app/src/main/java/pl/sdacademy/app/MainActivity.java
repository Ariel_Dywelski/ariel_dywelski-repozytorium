package pl.sdacademy.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText urlEditText;
    private Button button;
    public static final String KEY_URL = "url";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        urlEditText = (EditText) findViewById(R.id.editUrl);
        button = (Button) findViewById(R.id.nextButton);
        urlEditText.setSelection(urlEditText.getText().length());
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String url = urlEditText.getText().toString();

                Intent startSecondActivity = new Intent(MainActivity.this, WebActivity.class);
                startSecondActivity.putExtra(KEY_URL, url);
                startActivity(startSecondActivity);
            }
        });

    }
}
