package pl.sdacademy.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormActivity extends AppCompatActivity {

    @BindView(R.id.acceptButton)
    Button acceptButton;

    @BindView(R.id.userName)
    EditText userName;

    @BindView(R.id.userSurname)
    EditText userSurname;

    @BindView(R.id.userBirthDate)
    EditText userBirthday;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.userStreetAddress)
    EditText userStreet;

    @BindView(R.id.userNumberHouse)
    EditText userHouseNumber;

    @BindView(R.id.userPostalCode)
    EditText userPostalCode;

    @BindView(R.id.userCity)
    EditText userCity;

    @BindView(R.id.userCountry)
    EditText userCountry;

    @BindView(R.id.userEmail)
    EditText userEmail;

    @BindView(R.id.userPhoneNumber)
    EditText userPhoneNumber;

    @BindView(R.id.userPassword)
    EditText userPassword;

    @BindView(R.id.userPasswordConfirm)
    EditText userPasswordConfirm;

    @BindView(R.id.newsletterCheckBox)
    CheckBox newsletterCheckBox;

    @BindView(R.id.imageButton)
    ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        ButterKnife.bind(this);


        imageButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int event = motionEvent.getAction();

                switch (event) {
                    case MotionEvent.ACTION_DOWN:
                        userPassword.setTransformationMethod(null);
                        userPassword.setSelection(userPassword.getText().length());
                        return true;
                    case MotionEvent.ACTION_UP:
                        userPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        userPassword.setSelection(userPassword.getText().length());
                        return true;
                }
                return false;
            }
        });

    }

    public boolean isEmptyFieldInForm() {
        String nameUser = userName.getText().toString();
        String surnameUser = userSurname.getText().toString();
        String birthdayUser = userBirthday.getText().toString();
        String streetUser = userStreet.getText().toString();
        String houseNumberUser = userHouseNumber.getText().toString();
        String postalCodeUser = userPostalCode.getText().toString();
        String cityUser = userCity.getText().toString();
        String countryUser = userCountry.getText().toString();
        String emailUser = userEmail.getText().toString();
        String phoneNumberUser = userPhoneNumber.getText().toString();
        String passwordUser = userPassword.getText().toString();
        String passwordConfirmUser = userPasswordConfirm.getText().toString();


        return nameUser.isEmpty() || surnameUser.isEmpty()
                || birthdayUser.isEmpty() || streetUser.isEmpty() || houseNumberUser.isEmpty()
                || postalCodeUser.isEmpty() || cityUser.isEmpty() || countryUser.isEmpty()
                || emailUser.isEmpty() || phoneNumberUser.isEmpty() || passwordUser.isEmpty()
                || passwordConfirmUser.isEmpty();
    }

    @OnClick(R.id.acceptButton)
    public void login(View view) {

        String message = getString(R.string.loginComunicat);
        String messageAccepted = getString(R.string.registerNewUser);
        if (isEmptyFieldInForm()) {
            Toast.makeText(FormActivity.this, message, Toast.LENGTH_LONG).show();
        } else {
            Intent acceptIntent = new Intent(FormActivity.this, LoginActivity.class);
            startActivity(acceptIntent);
            Toast.makeText(FormActivity.this, messageAccepted, Toast.LENGTH_LONG).show();
        }


    }
}
