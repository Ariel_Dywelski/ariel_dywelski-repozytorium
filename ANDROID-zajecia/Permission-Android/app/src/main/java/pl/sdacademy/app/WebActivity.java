package pl.sdacademy.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

public class WebActivity extends AppCompatActivity {
    private TextView urlText;
    private Button nextButton;
    private WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        urlText = (TextView) findViewById(R.id.webAddress);
        nextButton = (Button) findViewById(R.id.openButton);
        webView = (WebView) findViewById(R.id.webView);

        webView.setWebViewClient(new WebViewClient());


        Intent incomingIntent = getIntent();
        final String url = incomingIntent.getExtras().getString(MainActivity.KEY_URL);

        urlText.setText(url);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.loadUrl(url);
            }
        });


    }
}
