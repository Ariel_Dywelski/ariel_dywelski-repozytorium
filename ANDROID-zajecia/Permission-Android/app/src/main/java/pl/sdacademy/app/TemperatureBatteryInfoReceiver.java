package pl.sdacademy.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.widget.Toast;

/**
 * Created by ARIEL on 2016-11-29.
 */

public class TemperatureBatteryInfoReceiver extends BroadcastReceiver {

    int temp =  0;
    float getTemperature() {
        return temp;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
        float tempTwo = ((float)temp)/10;
        String message = "Current " + BatteryManager.EXTRA_TEMPERATURE + " = " + tempTwo + "\u00B0C";
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}