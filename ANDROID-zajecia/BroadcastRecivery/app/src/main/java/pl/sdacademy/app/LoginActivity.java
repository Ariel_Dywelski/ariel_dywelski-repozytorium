package pl.sdacademy.app;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    private static final int READ_WRITE_CONTACTS_REQUEST = 123;

    private Button loginButton;
    private TextView messageText;
    private TextView noConnectionMessageText;
    private ImageView logoImageView;

    private BroadcastReceiver networkConnectionReceiver;
    private IntentFilter networkConnecitonIntentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = (Button) findViewById(R.id.loginButton);
        messageText = (TextView) findViewById(R.id.permissionMessage);
        noConnectionMessageText = (TextView) findViewById(R.id.noConnectionMessage);
        logoImageView = (ImageView) findViewById(R.id.appLogo);

        if (dontHavePermission(Manifest.permission.READ_CONTACTS) || dontHavePermission(Manifest.permission.WRITE_CONTACTS)) {
            if (shoudShowPermissionRationale(Manifest.permission.READ_CONTACTS) && shoudShowPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                showRationaleDialog();
            } else {
                requestReadWriteContactsPermission();
            }
        } else {
            loginButton.setEnabled(true);
            messageText.setVisibility(View.INVISIBLE);
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startMainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(startMainActivityIntent);
            }
        });

        Animation rotation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.image_anim);
        logoImageView.startAnimation(rotation);

        networkConnectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (isConnected()) {
                    loginButton.setEnabled(true);
                    noConnectionMessageText.setVisibility(View.GONE);
                } else {
                    loginButton.setEnabled(false);
                    noConnectionMessageText.setVisibility(View.VISIBLE);
                }
            }
        };

        networkConnecitonIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(networkConnectionReceiver, networkConnecitonIntentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(networkConnectionReceiver);
        super.onPause();
    }

    private boolean dontHavePermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean shoudShowPermissionRationale(String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
    }

    private void requestReadWriteContactsPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS},
                READ_WRITE_CONTACTS_REQUEST);
    }

    private void showRationaleDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestReadWriteContactsPermission();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .create()
                .show();
    }

    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_WRITE_CONTACTS_REQUEST: {
                if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // nadano uprawnienie
                    loginButton.setEnabled(true);
                    messageText.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
