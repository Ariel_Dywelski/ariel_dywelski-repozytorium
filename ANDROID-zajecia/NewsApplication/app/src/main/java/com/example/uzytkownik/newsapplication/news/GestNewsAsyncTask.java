package com.example.uzytkownik.newsapplication.news;

import android.os.AsyncTask;

import java.util.List;


public class GestNewsAsyncTask extends AsyncTask<Void, Void, List<News>> {

    private final NewsService newsService;
    private final NewsDownloadedListener newsDownloadedListener;

    public GestNewsAsyncTask(NewsDownloadedListener newsDownloadedListener) {
        this.newsDownloadedListener = newsDownloadedListener;
        newsService = new NewsService();
    }

    @Override
    protected List<News> doInBackground(Void... voids) {
        try {
            return newsService.getNews();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<News> newses) {
        newsDownloadedListener.onNewsDownloaded(newses);
    }

    public interface NewsDownloadedListener {
        void onNewsDownloaded(List<News> news);
    }
}
