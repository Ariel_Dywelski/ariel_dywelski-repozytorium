package com.example.uzytkownik.newsapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.uzytkownik.newsapplication.news.GestNewsAsyncTask;
import com.example.uzytkownik.newsapplication.news.News;
import com.example.uzytkownik.newsapplication.news.NewsRecycleAdapter;

import java.util.List;

import static com.example.uzytkownik.newsapplication.R.id.news_recyclerView;

public class NewsActivity extends AppCompatActivity
        implements GestNewsAsyncTask.NewsDownloadedListener {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        recyclerView = (RecyclerView) findViewById(news_recyclerView);

        new GestNewsAsyncTask(this).execute();

    }

    @Override
    public void onNewsDownloaded(List<News> news) {
        if (news == null) {
            Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show();
            return;
        }

        Toast.makeText(this, "Downloaded: " + news.size(), Toast.LENGTH_LONG).show();

        NewsRecycleAdapter adapter = new NewsRecycleAdapter(this, news);

        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
