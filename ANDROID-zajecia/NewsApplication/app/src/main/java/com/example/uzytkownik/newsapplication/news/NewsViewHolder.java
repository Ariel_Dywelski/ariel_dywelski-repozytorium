package com.example.uzytkownik.newsapplication.news;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.uzytkownik.newsapplication.R;

/**
 * Created by ARIEL on 2016-12-15.
 */

public class NewsViewHolder extends RecyclerView.ViewHolder {
    public final TextView title;
    public final TextView content;
    public final ImageView image;

    public NewsViewHolder(View view){
        super(view);
        this.image = (ImageView) view.findViewById(R.id.image);
        this.title = (TextView) view.findViewById(R.id.title);
        this.content = (TextView) view.findViewById(R.id.content);


    }
}
