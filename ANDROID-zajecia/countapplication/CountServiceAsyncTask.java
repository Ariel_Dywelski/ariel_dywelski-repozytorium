package com.example.uzytkownik.countapplication;

import android.os.AsyncTask;
import android.util.Log;

public class CountServiceAsyncTask extends AsyncTask<Void, Integer, Void> {

    private final CountService countService;

    public void setListener(CountListener listener) {
        this.listener = listener;
    }

    private CountListener listener;
    private int value;

    public CountServiceAsyncTask(CountService countService) {
        this.countService = countService;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        for(int i =0; i<=100; i++){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress(i);
            Log.e("count", "count: "+i);
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        value = values[0];
        if(listener != null){
            listener.onProgress(value);
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        countService.stopSelf();
    }


}
