package com.example.uzytkownik.countapplication;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity implements CountListener {

    private CountService.CountServiceBinder binder;
    private SeekBar seekBar;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            binder = (CountService.CountServiceBinder) iBinder;
            binder.createListener(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            binder.clearListener();
            binder = null;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, CountService.class);
        startService(intent);
        bindService(intent, serviceConnection, BIND_AUTO_CREATE);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setProgress(0);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(binder != null){
            binder.clearListener();
        }
        unbindService(serviceConnection);
    }

    @Override
    public void onProgress(int progress) {
        seekBar.setProgress(progress);
    }
}
