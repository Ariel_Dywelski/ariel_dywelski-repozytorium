package com.example.uzytkownik.countapplication;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class CountService extends Service {

    private CountServiceAsyncTask countServiceAsyncTask;

    @Override
    public void onCreate() {
        super.onCreate();
        countServiceAsyncTask = new CountServiceAsyncTask(this);
        countServiceAsyncTask.execute();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new CountServiceBinder();
    }

    public class CountServiceBinder extends Binder {
        public void createListener(CountListener countListener) {
            countServiceAsyncTask.setListener(countListener);
        }
        public void clearListener(){
            countServiceAsyncTask.setListener(null);
        }
    }
}
