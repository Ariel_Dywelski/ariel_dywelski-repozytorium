package com.example.uzytkownik.playerapplication;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class PlayerService extends Service {


    private MediaPlayer player;
    private float volume = 0.5f;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new PlayerServiceBinder();
    }

    public class PlayerServiceBinder extends Binder {
        public void startPlay() {
            if (isPlaying()){
                return;
            }
            player = MediaPlayer.create(PlayerService.this, R.raw.piosenka);
            player.start();
            player.setVolume(volume, volume);
        }

        public void pauseResumePlay() {
            if(isPlaying()) {
                player.pause();
                return;
            }
            if (player != null&& !player.isPlaying()){
                player.start();

            }
        }

        public void stopPlay() {
            if(isPlaying()) {
                player.stop();
                player.release();
                player = null;
            }
        }

        public void setVolume(float volumeValue){
            volume = (volumeValue / 100f);
            if(player !=null) {
                player.setVolume(volume, volume);
            }

        }
        public boolean isPlaying() {
            return player !=null && player.isPlaying();
        }
    }

   }
