package com.example.uzytkownik.tictactoeapplication;

/**
 * Created by ARIEL on 2016-12-05.
 */

public interface GameContract {

    interface GameView {
        void fillBoardViews(Boolean[] fieldValues);
        void showPlayerRoundMessage(boolean player);
        void showPlayerWinMessage(boolean player);
        void showDrawMessage();

    }

    interface GamePresenter {
        void onBoardFieldClick(int field);
        void onNewGame();

    }
}
