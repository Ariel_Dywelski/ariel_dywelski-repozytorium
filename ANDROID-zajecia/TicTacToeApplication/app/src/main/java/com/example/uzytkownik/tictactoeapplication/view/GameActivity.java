package com.example.uzytkownik.tictactoeapplication.view;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.uzytkownik.tictactoeapplication.GameContract;
import com.example.uzytkownik.tictactoeapplication.R;
import com.example.uzytkownik.tictactoeapplication.presenter.GamePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameActivity extends AppCompatActivity implements GameContract.GameView {

    @BindView(R.id.messageTurnPlayer)
    TextView messageText;

    @BindView(R.id.gameBoard)
    ViewGroup gameBoard;

    private GamePresenter gamePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        gamePresenter = new GamePresenter(this);



    }

    @Override
    public void fillBoardViews(Boolean[] fieldValues) {
        for (int i = 0; i < fieldValues.length; i++) {
            String tagToFind = String.valueOf(i);

            TextView field = (TextView) gameBoard.findViewWithTag(tagToFind);
            String playerName = getPlayerName(fieldValues[i]);
            field.setText(playerName);
        }
    }

    @Override
    public void showPlayerRoundMessage(boolean player) {
        String playerName = getPlayerName(player);
        String message = getString(R.string.player_turn_info, playerName);
        int textColor = ContextCompat.getColor(this, R.color.colorPrimary);
        messageText.setTextColor(textColor);
        messageText.setText(message);
    }

    @Override
    public void showPlayerWinMessage(boolean player) {
        String playerName = getPlayerName(player);
        String message = getString(R.string.player_win_message, playerName);
        int textColor = ContextCompat.getColor(this, R.color.colorWinner);
        messageText.setTextColor(textColor);
        messageText.setText(message);
    }

    @Override
    public void showDrawMessage() {
        int textColor = ContextCompat.getColor(this, R.color.colorDraw);
        messageText.setTextColor(textColor);
        messageText.setText(R.string.player_draw_message);
    }

    @OnClick({R.id.field0, R.id.field1, R.id.field2, R.id.field3, R.id.field4, R.id.field5, R.id.field6, R.id.field7, R.id.field8})
    public void onFieldClick(View view) {
        String tag = view.getTag().toString();
        int fieldNumber = Integer.parseInt(tag);
        gamePresenter.onBoardFieldClick(fieldNumber);
    }

    @OnClick(R.id.newGameButton)
    public void startNewGame() {
        gamePresenter.onNewGame();
    }


    private String getPlayerName(Boolean value) {
        if (value == null) {
            return "";
        }
            else if (value) {
            return getString(R.string.player_0);
        } else {
            return getString(R.string.player_1);
        }
    }
}
