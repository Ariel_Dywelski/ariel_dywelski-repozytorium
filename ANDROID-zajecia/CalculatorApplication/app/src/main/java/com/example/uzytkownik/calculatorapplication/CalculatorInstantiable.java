package com.example.uzytkownik.calculatorapplication;

public class CalculatorInstantiable {


    private static String SIGN_ADD = "+";
    private static String SIGN_SUB = "-";
    private static String SIGN_MUL = "*";
    private static String SIGN_DIV = "/";

    public boolean validateOperation(String line) {
        int foundSign = 0;
        if (line.contains(SIGN_ADD)) {
            foundSign++;
        }
        if (line.contains(SIGN_SUB)) {
            foundSign++;
        }
        if (line.contains(SIGN_MUL)) {
            foundSign++;
        }
        if (line.contains(SIGN_DIV)) {
            foundSign++;
        }
        if (foundSign != 1) {
            return false;
        }
        return true;
    }

    public double calculateValue(String line) throws Exception {
        if (!validateOperation(line)) {
            throw new Exception("Invalid equation entry");
        }

        if (line.contains(SIGN_ADD)) {
            return parseAndCountAdd(line);
        } else if (line.contains(SIGN_SUB)) {
            return parseAndCountSub(line);
        } else if (line.contains(SIGN_MUL)) {
            return parseAndCountMul(line);
        } else {
            return parseAndCountDiv(line);
        }
    }

    public double parseAndCountSub(String line) {
        String[] splits = line.split("\\" + SIGN_SUB);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);

        return subtraction(firstValue, secondValue);
    }

    public double parseAndCountDiv(String line) throws Exception {
        String[] splits = line.split("\\" + SIGN_DIV);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);

        if (secondValue == 0.0) {
            throw new Exception("You can not divide by 0 !!!");
        }

        return divide(firstValue, secondValue);
    }

    public double parseAndCountAdd(String line) {
        String[] splits = line.split("\\" + SIGN_ADD);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);


        return add(firstValue, secondValue);
    }

    public double parseAndCountMul(String line) {
        String[] splits = line.split("\\" + SIGN_MUL);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);

        return multiply(firstValue, secondValue);
    }

    public double multiply(double firstValue, double secondValue) {
        return firstValue * secondValue;
    }

    public double add(double firstValue, double secondValue) {
        return firstValue + secondValue;
    }

    public double divide(double firstValue, double secondValue) {
        return firstValue / secondValue;
    }

    public double subtraction(double firstValue, double secondValue) {
        return firstValue - secondValue;
    }
}


