package com.example.uzytkownik.calculatorapplication;

public class Calculator {

    private static String SIGN_ADD = "+";
    private static String SIGN_SUB = "-";
    private static String SIGN_MUL = "*";
    private static String SIGN_DIV = "/";

    private static boolean validateOperation (String line){
        int foundSign = 0;
        if (line.contains(SIGN_ADD)){
            foundSign++;
        }
        if (line.contains(SIGN_SUB)){
            foundSign++;
        }
        if (line.contains(SIGN_MUL)){
            foundSign++;
        }
        if (line.contains(SIGN_DIV)){
            foundSign++;
        }
        if (foundSign != 1){
            return false;
        }
        return true;
    }

    public static double calculateValue (String line) throws Exception {
        if (!validateOperation(line)) {
            throw new Exception("Invalid equation entry");
        }

        if (line.contains(SIGN_ADD)) {
            return parseAndCountAdd(line);
        } else if (line.contains(SIGN_SUB)) {
            return parseAndCountSub(line);
        } else if (line.contains(SIGN_MUL)) {
            return parseAndCountMul(line);
        } else {
            return parseAndCountDiv(line);
        }
    }

    private static double parseAndCountSub (String line){
        String [] splits = line.split("\\"+SIGN_SUB);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);

        return subtraction(firstValue, secondValue);
    }
    private static double parseAndCountDiv (String line) throws Exception {
        String [] splits = line.split("\\"+SIGN_DIV);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);

        if (secondValue == 0.0){
            throw new Exception("You can divide by 0 !!!");
        }

        return divide(firstValue, secondValue);
    }
    private static double parseAndCountAdd (String line) {
        String [] splits = line.split("\\"+SIGN_ADD);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);


        return add(firstValue, secondValue);
    }
    private static double parseAndCountMul (String line){
        String [] splits = line.split("\\"+SIGN_MUL);

        double firstValue = Double.parseDouble(splits[0]);
        double secondValue = Double.parseDouble(splits[1]);

        return multiply(firstValue, secondValue);
    }

    private static double multiply(double firstValue, double secondValue) {
        return firstValue*secondValue;
    }

    private static double add(double firstValue, double secondValue) {
        return firstValue+secondValue;
    }

    private static double divide(double firstValue, double secondValue) {
        return firstValue/secondValue;
    }

    private static double subtraction(double firstValue, double secondValue) {
        return firstValue-secondValue;
    }
}
