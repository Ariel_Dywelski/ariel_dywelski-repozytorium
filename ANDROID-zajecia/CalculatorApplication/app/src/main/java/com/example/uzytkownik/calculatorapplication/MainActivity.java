package com.example.uzytkownik.calculatorapplication;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.input_Text)
    EditText inputValue;

    @BindView(R.id.button_0)
    Button button0;
    @BindView(R.id.button_1)
    Button button1;
    @BindView(R.id.button_2)
    Button button2;
    @BindView(R.id.button_3)
    Button button3;
    @BindView(R.id.button_4)
    Button button4;
    @BindView(R.id.button_5)
    Button button5;
    @BindView(R.id.button_6)
    Button button6;
    @BindView(R.id.button_7)
    Button button7;
    @BindView(R.id.button_8)
    Button button8;
    @BindView(R.id.button_9)
    Button button9;



    @OnClick({R.id.button_0, R.id.button_1, R.id.button_2, R.id.button_3, R.id.button_4,
            R.id.button_5, R.id.button_6, R.id.button_7, R.id.button_8, R.id.button_9,
            R.id.button_add, R.id.button_diff, R.id.button_multiply, R.id.button_div})
    void onClick(View v) {
        if(v instanceof  Button){
            Button thisButton = (Button) v;
            String text = thisButton.getText().toString();

            inputValue.setText(inputValue.getText() + text);
        }
    }

    @OnClick({R.id.button_result})
    void onClickResult (View v){
        try {
            inputValue.setText(String.valueOf(Calculator.calculateValue(inputValue.getText().toString())));
        } catch (Exception e) {
            inputValue.setText("Invalid Value");
        }
    }
    @OnClick(R.id.button_clean)
    void onClickClean (View v){
        inputValue.setText("");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }
}
