package com.example.uzytkownik.calculatorapplication;

import junit.framework.Assert;

import org.junit.Test;

public class CalculatorParserTest {

    @Test
    public void testParseSubFine() {
        CalculatorInstantiable calculator = new CalculatorInstantiable();
        double result = calculator.parseAndCountSub("20-30");
        double result1 = calculator.parseAndCountSub("1000-30");
        double result2 = calculator.parseAndCountSub("30-30");
        double result3 = calculator.parseAndCountSub("330-30");

        Assert.assertEquals(-10.0, result);
        Assert.assertEquals(970.0, result1);
        Assert.assertEquals(0.0, result2);
        Assert.assertEquals(300.0, result3);
    }

    @Test
    public void testParseDividedFine() {
        CalculatorInstantiable calculator = new CalculatorInstantiable();
        Double result = null;
        try {
            result = calculator.parseAndCountDiv("10/2");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(5.0, result);

        Double result1 = null;
        try {
            result1 = calculator.parseAndCountDiv("2/4");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(0.5, result1);

        Double result2 = null;
        try {
            result2 = calculator.parseAndCountDiv("2/2");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(1.0, result2);
    }

    @Test
    public void testParseDividedByNull() {
        CalculatorInstantiable calculator = new CalculatorInstantiable();
        Double result = null;

        try {
            result = calculator.parseAndCountDiv("10/0");
        } catch (Exception e) {
            Assert.assertEquals("You can not divide by 0 !!!", e.getMessage());
        }
    }
}

