package com.example.uzytkownik.calculatorapplication;

import junit.framework.Assert;

import org.junit.Test;

public class TestCalculator {
    @Test
    public void calculatePlus_1() {
        CalculatorInstantiable calculator = new CalculatorInstantiable();
        Assert.assertEquals(calculator.add(1.0, 2.0), 3.0);
    }
    @Test
    public void calculateMinus_1(){
        CalculatorInstantiable calculator = new CalculatorInstantiable();
        Assert.assertEquals(calculator.subtraction(1.0, 2.0), -1.0);
    }
}
