package com.sda.android.browser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.widget.Toast.LENGTH_SHORT;

public class WebFirstActivity extends AppCompatActivity {

    private EditText urlEditText;
    private Button button;
    public static final String KEY_URL = "url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        urlEditText = (EditText) findViewById(R.id.editUrl);
        button = (Button) findViewById(R.id.nextButton);

        urlEditText.setSelection(urlEditText.getText().length());
        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                openWebView();
            }
        });

    }

    private void openWebView() {

        String url = urlEditText.getText().toString();



        if (!isValidUrlAddress(url)) {
            Toast.makeText(this, R.string.url_error, LENGTH_SHORT).show();
            return;
        }

        Intent startSecondActivity = new Intent(WebFirstActivity.this, WebSecondActivity.class);
        startSecondActivity.putExtra(KEY_URL, url);
        startActivity(startSecondActivity);


    }

    private boolean isValidUrlAddress(String url) {
        return !TextUtils.isEmpty(url) && (url.startsWith("http://") || (url.startsWith("https://")));
    }
}

