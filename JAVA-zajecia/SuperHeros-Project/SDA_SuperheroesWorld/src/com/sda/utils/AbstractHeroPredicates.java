package com.sda.utils;

import java.util.function.Predicate;

import com.sda.superheroes.AbstractHero;

public class AbstractHeroPredicates {
	public static Predicate<AbstractHero> isHeroNameBatman(){
		return e -> e.getName().equals("batman");
	}

	public static Predicate<AbstractHero> isHeroPowerGreaterThanParam(double param){
		return e -> e.getPower() > param;
	}
	
	public static Predicate<AbstractHero> isHeroPowerLowerThanParam(double param){
		return e -> e.getPower() < param;
	}
	
	public static Predicate<AbstractHero> isHeroPowerBetweenParams(double bottom, double top){
		return isHeroPowerGreaterThanParam(bottom).and(isHeroPowerLowerThanParam(top));
	}
}
