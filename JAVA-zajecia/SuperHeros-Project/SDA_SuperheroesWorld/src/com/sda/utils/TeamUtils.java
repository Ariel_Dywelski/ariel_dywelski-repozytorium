package com.sda.utils;

import java.util.function.Predicate;

import com.sda.storage.HeroStorage;
import com.sda.superheroes.AbstractHero;
import com.sda.teams.InvalidHeroTeamException;
import com.sda.teams.Team;
import com.sda.teams.TeamType;

public class TeamUtils {
	public static Team createTeam(TeamType type, String fileName) {
		Team team = new Team(type);
		Predicate<AbstractHero> isTeamCorrect = p -> p.getTeam().equals(type);
		
		HeroFactory.parseHeroesFromFile(fileName);
		
		HeroStorage
		.getWholeList()
		.stream()
		.filter(isTeamCorrect)
		.forEach(hero -> {
			try {
				team.addHeroToTeam(hero);
			} catch (InvalidHeroTeamException e) {
				System.out.println("Exception: "+e);
			}
		});
		
		return team;
	}
}
