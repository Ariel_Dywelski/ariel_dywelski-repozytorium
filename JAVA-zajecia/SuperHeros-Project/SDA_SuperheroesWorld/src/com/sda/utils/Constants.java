package com.sda.utils;

public class Constants {
	public static final int EXPECTED_ENTRY_SIZE = 6;
	public static final String HERO_ENTRY = "hero";
	public static final String VILLAIN_ENTRY = "villain";
	public static final int MAX_BASE_CAPACITY = 5;
}
