package com.sda.utils;

public class Entry {
	
	private String type;
	private String name;
	private String attack;
	private String defense;
	private String health;
	private String team;
	
	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getAttack() {
		return attack;
	}

	public String getDefense() {
		return defense;
	}

	public String getHealth() {
		return health;
	}

	public String getTeam() {
		return team;
	}

	public Entry(String[] entryParts){
		this.type = entryParts[0];
		this.name = entryParts[1];
		this.attack = entryParts[2];
		this.defense = entryParts[3];
		this.health = entryParts[4];
		this.team = entryParts[5];
	}
}
