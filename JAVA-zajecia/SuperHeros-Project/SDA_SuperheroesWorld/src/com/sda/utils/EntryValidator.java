package com.sda.utils;

import org.apache.commons.lang3.StringUtils;

import com.sda.teams.TeamType;

public class EntryValidator {

	private String[] entryParts;
	private Entry entry;

	public EntryValidator(String entry) {
		entryParts = entry.split(";");
	}

	public boolean isValid() {
		if(isEntrySizeCorrect()){
			this.entry = new Entry(entryParts);
		} else {
			return false;
		}
		return isEntryNameNotBlank() 
				&& isEntryStatsNumeric() 
				&& isEntryTeamTypeCorrect()
				&& isEntryTypeCorrect();
	}

	private boolean isEntrySizeCorrect() {
		if (entryParts.length != Constants.EXPECTED_ENTRY_SIZE) {
			return false;
		}
		return true;
	}

	private boolean isEntryTeamTypeCorrect() {
		if (!TeamType.contains(entry.getTeam())) {
			return false;
		}
		return true;
	}

	private boolean isEntryTypeCorrect() {
		if (!(entry.getType().equals(Constants.HERO_ENTRY) || entry.getType().equals(Constants.VILLAIN_ENTRY))) {
			return false;
		}
		return true;
	}

	private boolean isEntryStatsNumeric() {
		if (!(StringUtils.isNumeric(entry.getAttack()) && StringUtils.isNumeric(entry.getDefense())
				&& StringUtils.isNumeric(entry.getHealth()))) {
			return false;
		}
		return true;
	}

	private boolean isEntryNameNotBlank() {
		if (StringUtils.isBlank(entry.getName())) {
			return false;
		}
		return true;
	}
}
