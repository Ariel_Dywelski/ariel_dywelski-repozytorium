package com.sda.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.sda.exceptions.InvalidHeroEntryException;
import com.sda.storage.HeroStorage;
import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;
import com.sda.teams.TeamType;

public class HeroFactory {
	private final static Logger LOGGER = Logger.getLogger(HeroFactory.class.getName());

	public static void printLineParts(String[] lineParts) {
		for (String part : lineParts) {
			System.out.print(part + " ");
		}
		System.out.println();
	}

	public static void parseHeroesFromURL(URL url) {
		/*
		 * 
		 * 
		 * 
		 * 
		 */
	}

	public static void parseHeroesFromFile(String fileName) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;

			while ((line = br.readLine()) != null) {
				if(isHeroEntryValid(line)){
					parseHeroFromStringArray(line.split(";"));
				}
				
			}
			br.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	static void parseHeroFromStringArray(String[] lineParts) {
	
//		Alternative way:
//		AbstractHero hero = createAbstractHero(lineParts, lineParts[0].equals(Constants.HERO_ENTRY));		
//		if(lineParts[0].equals(Constants.HERO_ENTRY)){
//			hero = createAbstractHero(lineParts, true);
//		} else {
//			hero = createAbstractHero(lineParts, false);
//		}
//		HeroStorage.addToStorage(hero);
		
		HeroStorage.addToStorage(createAbstractHero(lineParts, lineParts[0].equals(Constants.HERO_ENTRY)));
	}

	//type;name;attack;defense;health;team
	
	private static HeroStatistics createStatisticsBasedOnStringArray(String[] lineParts) {
		return new HeroStatistics(Integer.parseInt(lineParts[4]), Integer.parseInt(lineParts[2]), Integer.parseInt(lineParts[3]));
	}
	
	private static AbstractHero createAbstractHero(String[] lineParts, boolean isHero){
		String name = lineParts[1];
		HeroStatistics stats = createStatisticsBasedOnStringArray(lineParts);
		TeamType type = TeamType.valueOf(lineParts[5]);
		if(isHero){
			return new SuperHero(name, stats, type);
		} else {
			return new Villain(name, stats, type);
		}
	}
	
	
	
	

	public static void addHeroEntryToFile(String filename, String heroEntry) throws InvalidHeroEntryException {
		
		if(!isHeroEntryValid(heroEntry)){
			throw new InvalidHeroEntryException("Entry:\n"+heroEntry+" \nhas an incorrect format");
		}
		
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));

			out.println(heroEntry);

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Cannot write hero to file.", e);
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	static boolean isHeroEntryValid(String entry) {
		EntryValidator entryValidator = new EntryValidator(entry);
		return entryValidator.isValid();
	}

}
