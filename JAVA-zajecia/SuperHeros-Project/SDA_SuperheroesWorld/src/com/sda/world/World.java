package com.sda.world;

import java.util.ArrayList;
import java.util.List;

import com.sda.towns.Town;

public class World {
	private static List<Town> townsList;
	
	static{
		townsList = new ArrayList<>();
	}
	
	public static void addTownToWorld(Town town){
		townsList.add(town);
	}
}
