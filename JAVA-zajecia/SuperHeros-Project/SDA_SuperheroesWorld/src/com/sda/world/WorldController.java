package com.sda.world;

import java.io.IOException;
import java.util.logging.Logger;

import com.sda.exceptions.BaseCapacityExceededException;
import com.sda.teams.TeamType;
import com.sda.towns.Town;
import com.sda.utils.HeroCreator;
import com.sda.utils.PropertyReader;

public class WorldController {
	private final static Logger LOGGER = Logger.getLogger(WorldController.class.getName());

	public static void main(String[] args) throws IOException {
		 PropertyReader.loadPropertyValues();
		 String fileName = System.getProperty("config.superHeroConfigFile");
		
		Town gdansk = new Town("Gdansk");
		World.addTownToWorld(gdansk);
		try {
			gdansk.addHeroToTown(HeroCreator.createHeroWithDefaultStats("Czarek", TeamType.RED));
			gdansk.addHeroToTown(HeroCreator.createHeroWithDefaultStats("Batman", TeamType.RED));
		} catch (BaseCapacityExceededException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println(gdansk.getBaseNumberOfGivenType(TeamType.RED));
		System.out.println(gdansk.getNumberOfHeroesOfGivenType(TeamType.RED));

	}
}
