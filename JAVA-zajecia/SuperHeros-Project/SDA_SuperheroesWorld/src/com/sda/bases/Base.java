package com.sda.bases;

import java.util.ArrayList;
import java.util.List;

import com.sda.exceptions.BaseCapacityExceededException;
import com.sda.superheroes.AbstractHero;
import com.sda.utils.Constants;

public class Base {

	private List<AbstractHero> heroesList;
	
	public Base(){
		heroesList = new ArrayList<>();
	}
	
	public int getCurrentUsage(){
		return heroesList.size();
	}
	
	public void addHeroToBase(AbstractHero hero) throws BaseCapacityExceededException{
		if(Constants.MAX_BASE_CAPACITY == heroesList.size()){
			throw new BaseCapacityExceededException(
					"You cannot add hero to this base, because it is full");
		}
		
		heroesList.add(hero);
	}
}
