package com.sda.storage;

import java.util.ArrayList;
import java.util.List;

import com.sda.superheroes.AbstractHero;

public class HeroStorage {
	
	private static List<AbstractHero> heroes;
	
	static{
		heroes = new ArrayList<AbstractHero>();
	}
	
	public static void addToStorage(AbstractHero hero) {
		heroes.add(hero);
	}

	
	public static boolean isEmpty() {
		return heroes.isEmpty();
	}


	public static void printEntries() {

		heroes.stream().forEach(hero -> {
			System.out.println("Hero: "+hero.getName()+"| Power: "+hero.getPower()+ "| Team: "+hero.getTeam());
		});
	}
	
	public static List<AbstractHero> getWholeList() {
		return heroes;
	}
	
	public static void clearStorage(){
		heroes.clear();
	}
	

}
