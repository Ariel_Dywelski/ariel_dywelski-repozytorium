package com.sda.exceptions;

@SuppressWarnings("serial")
public class BaseCapacityExceededException extends Exception {
	public BaseCapacityExceededException(String message){
		super(message);
	}
}
