package com.sda.exceptions;

@SuppressWarnings("serial")
public class InvalidHeroEntryException extends Exception {

	public InvalidHeroEntryException(String arg0) {
		super(arg0);
	}

}
