package com.sda.towns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sda.bases.Base;
import com.sda.exceptions.BaseCapacityExceededException;
import com.sda.superheroes.AbstractHero;
import com.sda.teams.TeamType;
import com.sda.utils.Constants;

public class Town {
	private Map<TeamType, List<Base>> bases;
	private TeamType leaderType;
	private String townName;

	public Town(String townName) {
		this.townName = townName;
		bases = new HashMap<>();
		leaderType = TeamType.NONE;

		initializeBases();
	}

	private void initializeBases() {
		bases.put(TeamType.RED, new ArrayList<Base>());
		bases.put(TeamType.BLUE, new ArrayList<Base>());
		bases.put(TeamType.GREEN, new ArrayList<Base>());
	}

	public void addHeroToTown(AbstractHero hero) throws BaseCapacityExceededException {
		List<Base> currentHeroTeamList = bases.get(hero.getTeam());

		boolean isFreeBaseFound = false;

		for (Base base : currentHeroTeamList) {
			if (base.getCurrentUsage() < Constants.MAX_BASE_CAPACITY) {
				base.addHeroToBase(hero);  
				isFreeBaseFound = true;
				break;
			}
		}
		if (!isFreeBaseFound) {
			Base baseToAdd = new Base();
			baseToAdd.addHeroToBase(hero);
			currentHeroTeamList.add(baseToAdd);
		}

	}
	
	public int getBaseNumberOfGivenType(TeamType type){
		return bases.get(type).size();
	}
	
	public int getNumberOfHeroesOfGivenType(TeamType type){
		return bases.get(type).stream().mapToInt(e -> e.getCurrentUsage()).sum();
	}
}
