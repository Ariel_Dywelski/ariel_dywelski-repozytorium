package com.sda.utils;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.sda.storage.HeroStorage;
import com.sda.superheroes.AbstractHero;

public class HeroFactoryTest {

	@Test
	public void testIsHeroEntryValid(){
		Assert.assertTrue(HeroFactory.isHeroEntryValid("hero;a;5;6;7;RED"));
	}
	
	
	@Test
	public void testCreatingHero(){
		HeroFactory.parseHeroFromStringArray("hero;grazyna;5;6;7;RED".split(";"));
		List<AbstractHero> heroStorageList = HeroStorage.getWholeList();
		
		Assert.assertFalse(heroStorageList.isEmpty());
		Assert.assertTrue(heroStorageList.get(0).getName().equals("grazyna"));
	}
	
	@After
	public void cleanUp(){
		HeroStorage.clearStorage();
	}
	
	@Before
	public void setUp(){
		HeroStorage.clearStorage();
	}
	
}
