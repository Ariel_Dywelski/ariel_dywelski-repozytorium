package com.sda.utils;

import org.junit.Assert;
import org.junit.Test;

import com.sda.teams.Team;
import com.sda.teams.TeamType;


public class TeamUtilsTest {

	@Test
	public void testCreatingTeam(){
		
		TeamType type = TeamType.BLUE;
		String fileName = "C:\\Users\\Kasper\\heroes.txt";
		
		Team team = TeamUtils.createTeam(type, fileName);
		
		Assert.assertTrue(team.getTeamLeader().getName().equalsIgnoreCase("batman"));
		Assert.assertTrue(team.getTeamSize() == 5);
	}
}
