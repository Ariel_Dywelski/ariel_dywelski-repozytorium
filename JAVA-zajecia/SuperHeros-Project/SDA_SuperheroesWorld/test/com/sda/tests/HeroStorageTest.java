package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sda.storage.HeroStorage;
import com.sda.superheroes.HeroStatistics;
import com.sda.teams.TeamType;
import com.sda.utils.HeroCreator;


public class HeroStorageTest {
	
	@Before
	public void addToStorage(){
		HeroStorage.addToStorage(HeroCreator.createHero("batman", new HeroStatistics(2,3,4), TeamType.BLUE));
		HeroStorage.addToStorage(HeroCreator.createVillain("joker", new HeroStatistics(2,3,4), TeamType.GREEN));
	}
	
	@Test
	public void testPrintingEntries(){
		HeroStorage.printEntries();
	}
	
	@Test
	public void testCheckIfListEmpty(){
		HeroStorage.clearStorage();
		assertTrue(HeroStorage.isEmpty());
	}
	
	@After
	public void clearStorage(){
		HeroStorage.clearStorage();
	}

}
