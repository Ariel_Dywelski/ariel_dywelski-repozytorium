package com.sda.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.HeroStatistics;
import com.sda.teams.InvalidHeroTeamException;
import com.sda.teams.Team;
import com.sda.teams.TeamType;

@RunWith(MockitoJUnitRunner.class)
public class MockitoTest {

	@Mock
	AbstractHero hero;
	
	@Mock
	HeroStatistics stats;
	
	@Test
	public void testAddingHeroToTeam() throws InvalidHeroTeamException{
		Team team = new Team(TeamType.RED);
		Mockito.when(hero.getTeam()).thenReturn(TeamType.RED);
		Mockito.when(hero.getName()).thenReturn("Batman");

		team.addHeroToTeam(hero);
		Mockito.verify(hero, Mockito.times(1)).getName();
	}
	
	@Test
	public void doubleMock(){
		Mockito.when(stats.getAttack()).thenReturn(1000);
		
		Mockito.when(hero.getStats()).thenReturn(stats);
		
		System.out.println(hero.getStats().getAttack());
		
		Mockito.when(stats.getAttack()).thenReturn(2000);
		
		System.out.println(hero.getStats().getAttack());
	}
}
