package com.sda.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.sda.storage.HeroStorage;
import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.HeroStatistics;
import com.sda.teams.TeamType;
import com.sda.utils.AbstractHeroPredicates;
import com.sda.utils.HeroCreator;

public class StreamTest {

	@Before
	public void addToStorage() {

		String fileName = "C:\\Users\\Kasper\\heroes.txt";

		HeroStorage.addToStorage(HeroCreator.createHero("batman", new HeroStatistics(2, 3, 4), TeamType.BLUE));
		HeroStorage.addToStorage(HeroCreator.createVillain("joker", new HeroStatistics(2, 3, 4), TeamType.GREEN));
	}

	@Test
	public void testFindingBatman() {

		List<AbstractHero> result = HeroStorage.getWholeList().stream()
				.filter(AbstractHeroPredicates.isHeroNameBatman()).collect(Collectors.toList());

		Assert.assertFalse(result.isEmpty());

		result.stream().forEach(e -> {
			System.out.println(e.getName());
		});
	}

	@Test
	public void testPowerPredicate() {

		List<AbstractHero> result = HeroStorage.getWholeList().stream()
				.filter(AbstractHeroPredicates.isHeroPowerGreaterThanParam(120)).collect(Collectors.toList());
		Assert.assertFalse(result.isEmpty());

		result.stream().forEach(e -> {
			System.out.println(e.getName() + " " + e.getPower());
		});

	}

	@Test
	public void testSumOfPowers() {

		double sum = HeroStorage.getWholeList().stream().mapToDouble(AbstractHero::getPower).sum();

		System.out.println("Sum is: " + sum);
	}

	@Test
	public void testDistinctNumbers() {
		List<Integer> numbersList = new ArrayList<>();
		numbersList.add(6);
		numbersList.add(4);
		numbersList.add(3);
		numbersList.add(3);
		numbersList.add(3);
		numbersList.add(4);

		numbersList.stream().distinct().forEach(System.out::println);

	}

	@Test
	public void testStreamOnSingleton() {
		AbstractHero hero = HeroCreator.createHeroWithDefaultStats("Dave", TeamType.BLUE);

		Collections.singleton(hero).stream().filter(e -> e.getName().equals("Dave")).forEach(e -> {
			System.out.println("Name is: " + e.getName());
		});

	}

	@After
	public void clearStorage() {
		HeroStorage.clearStorage();
	}
}
