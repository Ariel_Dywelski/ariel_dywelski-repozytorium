package com.sda.tests;

import org.junit.Test;

import com.sda.teams.InvalidHeroTeamException;
import com.sda.teams.Team;
import com.sda.teams.TeamType;
import com.sda.utils.HeroCreator;

public class TeamTest {

	@Test(expected = InvalidHeroTeamException.class)
	public void testExceptionWhileAddingWrongHero() throws InvalidHeroTeamException {
		Team team = new Team(TeamType.RED);
		team.addHeroToTeam(HeroCreator.createHeroWithDefaultStats("Hero", TeamType.RED));
	}

	@Test
	public void testNoExceptionWhileAddingHero() throws InvalidHeroTeamException {
		Team team = new Team(TeamType.GREEN);

		team.addHeroToTeam(HeroCreator.createVillainWithDefaultStats("Harry", TeamType.GREEN));
		team.addHeroToTeam(HeroCreator.createVillainWithDefaultStats("Barry", TeamType.GREEN));
		team.addHeroToTeam(HeroCreator.createVillainWithDefaultStats("Darry", TeamType.GREEN));
		team.addHeroToTeam(HeroCreator.createVillainWithDefaultStats("Tarry", TeamType.GREEN));
	}
}
