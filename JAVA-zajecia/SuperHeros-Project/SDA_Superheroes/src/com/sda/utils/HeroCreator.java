package com.sda.utils;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;
import com.sda.teams.TeamType;

public class HeroCreator {
	
	public static SuperHero createHero(String name, HeroStatistics stats, TeamType team){
		/*
		 * createHero method only creates instance of SuperHero based on given parameters
		 */
		return new SuperHero(name, stats, team);
	}
	
	public static SuperHero createHeroOfTeamRed(String name, HeroStatistics stats){
		return createHero(name,stats,TeamType.RED);
	}
	
	public static Villain createVillainOfTeamRed(String name, HeroStatistics stats){
		return createVillain(name,stats,TeamType.RED);
	}
	
	public static SuperHero createHeroOfTeamBlue(String name, HeroStatistics stats){
		return createHero(name,stats,TeamType.BLUE);
	}
	
	public static Villain createVillainOfTeamBlue(String name, HeroStatistics stats){
		return createVillain(name,stats,TeamType.BLUE);
	}
	
	public static Villain createVillain(String name, HeroStatistics stats, TeamType team){
		/*
		 * createVillain method only creates instance of Villain based on given parameters
		 */
		return new Villain(name, stats, team);
	}
	
	public static SuperHero createHeroWithDefaultStats(String name, TeamType team){
		/*
		 * loadPropertyValues():
		 * 
		 * Read all properties from config.properties file
		 * and set them as System properties, so they will be 
		 * accessible from code using System.getProperty("propertyName");
		 * 
		 * Integer.parseInt("someString") - parse integer number from given String
		 * 
		 */
		PropertyReader.loadPropertyValues();
		
		/*
		 * Default values as listed in config.properties file
		 */
		int actualAttack = Integer.parseInt(System.getProperty("config.superHeroBaseAttack"));
		int actualDefense = Integer.parseInt(System.getProperty("config.superHeroBaseDefense"));
		int actualHealth = Integer.parseInt(System.getProperty("config.superHeroBaseHealth"));
		
		/*
		 * Creating stats object based on default stats
		 */
		HeroStatistics stats =  new HeroStatistics(actualHealth, actualAttack, actualDefense);
		
		/*
		 * Calling createHero method that would return SuperHero instance based on given parameters
		 */
		return createHero(name, stats, team);
	}
	
	public static Villain createVillainWithDefaultStats(String name, TeamType team){
		/*
		 * loadPropertyValues():
		 * 
		 * Read all properties from config.properties file
		 * and set them as System properties, so they will be 
		 * accessible from code using System.getProperty("propertyName");
		 * 
		 * Integer.parseInt("someString") - parse integer number from given String
		 * 
		 */
		PropertyReader.loadPropertyValues();
		
		/*
		 * Default values as listed in config.properties file
		 */
		int actualAttack = Integer.parseInt(System.getProperty("config.superHeroBaseAttack"));
		int actualDefense = Integer.parseInt(System.getProperty("config.superHeroBaseDefense"));
		int actualHealth = Integer.parseInt(System.getProperty("config.superHeroBaseHealth"));
		
		/*
		 * Creating stats object based on default stats
		 */
		HeroStatistics stats =  new HeroStatistics(actualHealth, actualAttack, actualDefense);
		
		/*
		 * Calling createVillain method that would return Villain instance based on given parameters
		 */
		return createVillain(name, stats, team);
	}
	
	
	
}
