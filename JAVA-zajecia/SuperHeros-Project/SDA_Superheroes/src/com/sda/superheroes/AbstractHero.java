package com.sda.superheroes;

import com.sda.teams.TeamType;

public abstract class AbstractHero {

	private String name;
	private HeroStatistics stats;
	private TeamType team;
	private boolean isDead = false;
	

	public AbstractHero(String name, HeroStatistics stats, TeamType team){
		this.name = name;
		this.stats = stats;
		this.team = team;
		int bonusStat = 50;
		
		/*
		 * Adding some bonusStats depending on team value
		 * 
		 * RED - health
		 * BLUE - attack
		 * GREEN - defense
		 * 
		 */
		switch(team){
		case RED:
			stats.addToHealth(bonusStat);
			break;
		case BLUE:
			stats.addToAttack(bonusStat);
			break;
		case GREEN:
			stats.addToDefense(bonusStat);
			break;
			default:
		}
	}
	
	
	public void takeDamage(int damage){
		stats.substractFromHealth(damage);
		
		if(stats.getHealth() <= 0){
			isDead = true;
		}
	}
	
	public boolean isDead(){
		return isDead;
	}
	
	public String getName(){
		return name;
	}
	
	public HeroStatistics getStats(){
		return stats;
	}
	
	public TeamType getTeam(){
		return team;
	}
	
	/*
	 * Abstract method without implementation - 
	 * every child class will have to implement inherited method
	 */
	public abstract double getPower();
	
}
