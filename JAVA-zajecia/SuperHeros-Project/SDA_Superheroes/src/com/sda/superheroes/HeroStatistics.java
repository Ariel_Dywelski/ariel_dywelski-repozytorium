package com.sda.superheroes;

public class HeroStatistics {
	private int health;
	private int attack;
	private int defense;

	public HeroStatistics(int health, int attack, int defense){
		this.health = health;
		this.attack = attack;
		this.defense = defense;
	}
	
	public void substractFromHealth(int toSubstract){
		health-=toSubstract;
	}
	
	public int getHealth(){
		return health;
	}
	
	public int getAttack(){
		return attack;
	}
	
	public int getDefense(){
		return defense;
	}
	
	public void addToHealth(int toAdd){
		this.health+= toAdd;
	}
	
	public void addToAttack(int toAdd){
		this.attack+= toAdd;
	}
	
	public void addToDefense(int toAdd){
		this.defense+= toAdd;
	}
	
}
