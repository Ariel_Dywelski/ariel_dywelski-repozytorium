package com.sda.superheroes;

import com.sda.teams.TeamType;

public class SuperHero extends AbstractHero{

	/*
	 * Construtor must be explicitly defined as AbstractHero lacks default construtor.
	 * 
	 * AbstractHero has one constructor that takes 3 parameters
	 */
	
	public SuperHero(String name, HeroStatistics stats, TeamType team) {
		super(name, stats, team);
	}

	@Override
	public double getPower() {
		HeroStatistics tmp = getStats();
		double attack = tmp.getAttack();
		double health = tmp.getHealth();
		double defense = tmp.getDefense();
		
		return (defense + attack) * health;
	}
}
