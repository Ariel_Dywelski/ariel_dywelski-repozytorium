package com.sda.teams;

@SuppressWarnings("serial")
public class InvalidHeroTeamException extends Exception {
	public InvalidHeroTeamException(String message){
		super(message);
	}
}
