package com.sda.teams;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;

public class Team {
	private TeamType type = TeamType.NONE;
	private List<AbstractHero> heroesList;
	private AbstractHero teamLeader;
	private boolean buffAvailable = true;
	private Side side = Side.UNKNOWN;

	public Team(TeamType type) {
		this.type = type;
		this.heroesList = new ArrayList<AbstractHero>();
	}

	public AbstractHero getRandomHero() {

		if (!heroesList.isEmpty()) {
			int index = new Random().nextInt(heroesList.size());
			return heroesList.get(index);
		} 

		return null;
	}

	public void addHeroToTeam(AbstractHero hero) throws InvalidHeroTeamException {
		/*
		 * if hero team matches current TeamType, then we can add this hero to
		 * our team else - error message and status false
		 */
		if (hero.getTeam().equals(type)) {
			heroesList.add(hero);
			findTeamLeader();
			updateTeamSide();

		} else {
			throw new InvalidHeroTeamException(
					"This hero cannot be added to this team. Expected: " + this.type + " | Got: " + hero.getTeam());

		}
	}

	public AbstractHero getTeamLeader() {
		return teamLeader;
	}

	private void findTeamLeader() {
		/*
		 * Algorithm:
		 * 
		 * Assume that first hero is the strongest one if next hero is stronger
		 * - swap do this for each entry in List
		 * 
		 * if next hero name is Batman then there is no need to go further as it
		 * is obvious that Batman is the leader
		 * 
		 */
		AbstractHero leaderCandidate = heroesList.get(0);

		for (AbstractHero hero : heroesList) {

			if (hero.getPower() > leaderCandidate.getPower()) {
				leaderCandidate = hero;
			}

			if (hero.getName().equalsIgnoreCase("Batman")) {
				leaderCandidate = hero;
				break;
			}
		}

		this.teamLeader = leaderCandidate;
	}
	
	public int getTeamSize(){
		
		return heroesList.size();
	}

	@Override
	public String toString() {
		/*
		 * Using StringBuffer as it is one object, while using += on String
		 * creates new object each time - good practice
		 */
		boolean leaderFound = false;
		StringBuilder sb = new StringBuilder();

		for (AbstractHero hero : heroesList) {

			sb.append(hero.getName());
			if (hero.getName().equals(teamLeader.getName()) && hero.getPower() == teamLeader.getPower() && !leaderFound) {
				sb.append(" >LEADER<");
				leaderFound=!leaderFound;
			}
			sb.append("\n");

		}

		return sb.toString();
	}

	public double getTeamPower() {
		/*
		 * Team Power stands for each hero power summed up
		 */
		double powerSum = 0;

		for (AbstractHero hero : heroesList) {
			powerSum += hero.getPower();
		}
		return powerSum;

	}

	public void increaseTeamPowerWithAdditionalBuff() {

		if (buffAvailable) {

			int bonusStat = 10;

			for (AbstractHero hero : heroesList) {

				// [for each: if->Villain->addToHealth(10);
				// if->SuperHero->addToDefense(10);]

				if (hero instanceof SuperHero) {

					hero.getStats().addToDefense(bonusStat);

				} else if (hero instanceof Villain) {

					hero.getStats().addToHealth(bonusStat);

				}

			}

			buffAvailable = !buffAvailable;

		} else {
			System.out.println("There is no buff available.");
		}
	}

	public boolean isEmpty() {
		return heroesList.isEmpty();
	}

	private void updateTeamSide() {
		/*
		 * Count amount of SuperHeroes and Villains
		 * 
		 * Set side of our Team based on which side contains more heroes
		 */

		int countSuperHeroes = 0;
		int countVillains = 0;

		for (AbstractHero hero : heroesList) {
			if (hero instanceof SuperHero) {
				countSuperHeroes++;
			} else if (hero instanceof Villain) {
				countVillains++;
			}
		}

		if (countSuperHeroes > countVillains) {
			this.side = Side.GOOD;
		} else if (countSuperHeroes < countVillains) {
			this.side = Side.EVIL;
		} else {
			this.side = Side.UNKNOWN;
		}

	}

	public Side getSide() {
		return side;
	}
	
	public List<AbstractHero> getAliveHeroes(){
		List<AbstractHero> aliveHeroes = new ArrayList<AbstractHero>();
		
		for(AbstractHero hero : heroesList){
			if(!hero.isDead()){
				aliveHeroes.add(hero);
			}
		}
		
		return aliveHeroes;
	}
	
}
