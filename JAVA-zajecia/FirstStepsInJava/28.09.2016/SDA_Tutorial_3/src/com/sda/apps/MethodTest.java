package com.sda.apps;

public class MethodTest {

	public static void main(String[] args) {
		//a
		multiplyTwoNumbers(10, 20);
		
		//b
		divideGivenNumbers(10, 4);
		
		//c
		multiplyThreeNumbers(2, 3, 4);
		
		//d
		squaredNumber(subtractTwoNumbers(5.0, 2.0));
		
		//e
		printArrayWithMultiplication(new double[]{2.5, 1.4, 0.7, 5.6});
	}

	static void multiplyTwoNumbers(int firstNumber, int secondNumber){
		System.out.println("Multiplied two numbers: "+firstNumber*secondNumber);
	}
	
	static void divideGivenNumbers(double firstNumber, double secondNumber){
		System.out.println("Divided given numbers: "+firstNumber/secondNumber);
	}
	
	static void multiplyThreeNumbers(int firstNumber, int secondNumber, int thirdNumber){
		System.out.println("Multiplied three numbers: "+firstNumber*secondNumber*thirdNumber);
	}
	
	static double subtractTwoNumbers(double firstNumber, double secondNumber){
		return firstNumber-secondNumber;
	}
	
	static void squaredNumber(double numberToSquare){
		System.out.println("Squared Number is: "+numberToSquare*numberToSquare);
	}
	
	static void printArrayWithMultiplication(double[] array){
		for(int i=0; i<array.length; i++){
			System.out.print((array[i]*2.5)+" ");
		}
		System.out.println();
	}
}
