package buildMethod;

import java.util.Calendar;
import java.util.Random;

import javax.xml.crypto.Data;

public class LuckyNumber {

	public int number;

	public LuckyNumber(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "LuckyNuber [number=" + number + "]";
	}

	public static LuckyNumber fromString(String number) {
		return new LuckyNumber(Integer.parseInt(number));
	}

	public static LuckyNumber random(int max) {
		return new LuckyNumber(new Random().nextInt(max));
	}

	public static LuckyNumber random() {
		return new LuckyNumber(new Random().nextInt());
	}

	public static void Kalendarz(){
		Calendar calendar = Calendar.getInstance();
		Data data = (Data) calendar.getTime();
System.out.println(data);
	}


}
