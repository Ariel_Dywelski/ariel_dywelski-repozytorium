package buildMethod;

public class LuckyNumberMain {

	public static void main(String[] args) {
	
		System.out.println(new LuckyNumber(1));
		System.out.println(LuckyNumber.fromString("34"));

		System.out.println(LuckyNumber.random());
		System.out.println(LuckyNumber.random(36));
		
		
	}

}
