package zadanie1;

import java.util.ArrayList;
import java.util.List;

public class Box<T> {

	private List <T> values = new ArrayList<T>();

	public void add(T value) {
		values.add(value);
	}

	public T get() {
		return (T) values;
	}

}
