package zadanie1;

public class Adress {

	private final String city;

	public Adress(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Adress City: " + city + "";
	}
	
	
	
}
