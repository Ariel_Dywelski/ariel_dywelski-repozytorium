package zadanie1;

import java.util.Arrays;
import java.util.List;

public class MainPerson {

	public static void main(String[] args) {

		List<Adress> adress = Arrays.asList(new Adress("Gda�sk"));
		Person person = new Person("Jan", "Kowalski", adress);

		Person newPerson = changeName(person);

		System.out.println("Przed: " + person);
		System.out.println("Po: " + newPerson);
	}

	private static Person changeName(Person person) {
		Person newPerson = new Person("Piotr", person.getSurname(), person.getAddressList());
		return newPerson;
	}
}
