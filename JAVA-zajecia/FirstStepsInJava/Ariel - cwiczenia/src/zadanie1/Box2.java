package zadanie1;

import java.util.ArrayList;
import java.util.List;

public class Box2<T extends Valuable> {

	private List<T> values = new ArrayList<T>();

	public void add(T value) {
		values.add(value);
	}

	public T removeAndGet() {
		return values.remove(values.size() - 1);

	}

	public int calulateLoop() {
		int sum = 0;

		for (T value : values) {
			sum = sum + value.getValue();
		}

		return sum;

	}

	public int calculate() {
		return values.stream().map(Valuable::getValue).reduce(0, (sum, value) -> sum += value);
	}

	@Override
	public String toString() {
		return values.toString();
	}

}
