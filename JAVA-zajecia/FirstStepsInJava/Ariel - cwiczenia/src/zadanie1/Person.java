package zadanie1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class Person {

	private final String name;
	private final String surname;
	private final List<Adress> adressList;

	public Person(String name, String surname, List<Adress> adressList) {
		this.name = name;
		this.surname = surname;
		
		ArrayList<Adress> newList = new ArrayList<>(adressList);
 		this.adressList = Collections.unmodifiableList(newList);

	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
	
	public List<Adress> getAddressList() {
		return adressList;
	}

	@Override
	public String toString() {
		return "Person: [name:" + name+"]" + ", [surname=" + surname+"]" + ", " + adressList + "";
	}


}
