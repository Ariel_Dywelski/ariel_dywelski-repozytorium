package file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class FileMain {
	public static void main(String[] args) throws IOException {
		File file = new File("C:\\Users\\RENT\\Desktop\\Ariel\\∆wiczeniaPliki\\plik2.txt");
		System.out.println("Czy istnieje: " + file.exists());
		file.createNewFile();

		System.out.println("A teraz: " + file.exists());

		// file.delete();

		String copyPath = "C:\\Users\\RENT\\Desktop\\Ariel\\∆wiczeniaPliki\\plik3.txt";

		copy(file, copyPath);
	}

	private static void copy(File file, String copyPath) throws
		 IOException {
		 Files.copy(file.toPath(), Paths.get(copyPath));
		 if (file.isDirectory()) {
		 for (File f : file.listFiles()) {
		 copy(f, copyPath + "/" + f.getName());
		 }

		try (BufferedWriter bufferedWriter = Files
				.newBufferedWriter(Paths.get("C:\\Users\\RENT\\Desktop\\Ariel\\∆wiczeniaPliki\\plik2.txt"))) {
			bufferedWriter.write("Hello world it's my");

			System.out.println("It's write in my file.");

		} catch (IOException e) {
			e.printStackTrace();
		}

		 try {
		 Stream<String> myStream = Files
		 .lines(Paths.get("C:\\Users\\RENT\\Desktop\\Ariel\\∆wiczeniaPliki\\myFile.txt"));
		
		
		 myStream.forEach(System.out::println);
		
		 } catch (IOException e) {
		 e.printStackTrace();
		 }

		try {
			List<String> myLines = Files
					.readAllLines(Paths.get("C:\\Users\\RENT\\Desktop\\Ariel\\∆wiczeniaPliki\\myFile.txt"));

			for (int i = 1; i < myLines.size(); i = i + 2) {

				System.out.println(myLines.get(i));

			}
			String a = "a";
			for (int i = 0; i < myLines.size(); i++) {
				if (myLines.get(i).startsWith(a)) {
					System.out.println(myLines.get(i));
				}
			}
			System.out.println(myLines);
			
			}catch (IOException e) {
				e.printStackTrace();
				
			}

			

			try {
				String a = "a";
				List<String> lines = Files
						.readAllLines(Paths.get("C:\\Users\\RENT\\Desktop\\Ariel\\∆wiczeniaPliki\\myFile.txt"));
				for (int i = 0; i < lines.size(); i++) {
					String newLine = lines.get(i).replaceAll(a, "b");
					System.out.println(newLine);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		
		
		try{
			List<String> lines = Files
					.readAllLines(Paths.get("C:\\Users\\RENT\\Desktop\\Ariel\\∆wiczeniaPliki\\myFile.txt"));
			for (String line : lines) {
				long count = line.chars()
						.filter(c -> 'a' == c)
						.count();					
			
			System.out.println(count);
			}
			
		}catch (IOException e) {
			e.printStackTrace();
			
		}catch (Exception exception) {

		}
	
	}
}
}
