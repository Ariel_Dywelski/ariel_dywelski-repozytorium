package zadanie;

public class Fibbonaci {

	public static long fibonacci(int n) {
		if (n <= 1)
			return n;
		else
			return fibonacci(n - 1) + fibonacci(n - 2);
	}
	

	public static long fibonacci1 (int n){
		
		
			if(n==0){
				System.out.println(0);
				return 0;
			}
			if (n==1){
				System.out.println(1);
				return 1;
			}
			int post = 0;
			int ost = 1;
			for(int i = 2; i <= n; i++){
				int tmp = ost;
				ost = ost+post;
				post = tmp;
			}
			return ost;

	}

	public static void main(String[] args) {
		
		System.out.println(fibonacci(40));
		System.out.println(fibonacci1(40));
	}

}
