package zadanie;

public class MyNode {

	public String value;
	public MyNode next;

	public MyNode(String value) {
		this.value = value;
	}
	
	public boolean hasNext(){
		return next != null; 
	}
	

	@Override
	public String toString() {
		return value+" -> " +next;
	}
}
