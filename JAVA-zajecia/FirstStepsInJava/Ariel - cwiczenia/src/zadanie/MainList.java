package zadanie;

public class MainList {

	public static void main(String... a) {

		MyArrayList myList = new MyArrayList();
		myList.addToArray("1");
		myList.addToArray("2");
		myList.addToArray("3");
		myList.addToArray("4");
		myList.addToArray("5");
		// myList.addToArray("6");
		// System.out.println(myList.display());

		System.out.println(myList.toString());

		myList.removeFromList(3);
		System.out.println(myList);
		myList.removeFromList(2);
		System.out.println(myList);

		myList.addToArray("45");
		System.out.println(myList);
		
	
		System.out.println(myList.findIndexInMyArray("45"));
		System.out.println(myList.findIndexInMyArray("2"));
	}
}
