package zadanie;

import java.util.Objects;

public class Osoba {

	  	private String imie;
	    private String nazwisko;
	    private int adres;

	    public Osoba (String imie, String nazwisko, int adres) {

	        this.imie = imie;
	        this.nazwisko = nazwisko;
	        this.adres = adres;

	    }

	    @Override
	    public String toString() {
	        return "Person imie: " + imie + ", nazwisko: " + nazwisko + " Adres "+adres;
	    }

	    @Override
		public boolean equals(Object o){
			if (this == o){
				return true;
			}
			if (o == null || getClass() != o.getClass()){
				return false;
			}
			
			Osoba person = (Osoba) o;
			if(!Objects.equals(imie, person.imie)){
				return false;
			}
			if(!Objects.equals(nazwisko, person.nazwisko)){
				return false;
			}
			return true;
		}
	    
	    @Override
	    
	    public int hashCode(){
	    	int result = imie != null ? imie.hashCode()  : 0;
	    	int result1 = nazwisko != null ? nazwisko.hashCode() : 0;
	    	
	    	return result + result1; 
	    }
		
}