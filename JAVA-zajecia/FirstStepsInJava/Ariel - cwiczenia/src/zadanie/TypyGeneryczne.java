package zadanie;

public class TypyGeneryczne {

	public class MyArrayList<T> {

		private T[] table;
		private int size = 0;

		public MyArrayList(int maxSize) {
		        this.table = (T[]) new Object[maxSize];
		    
		    }

		public void add(T value) {
		        table[size++] = value;
		    }

		public T get(int index) {
			return table[index];
		}

		public int size() {
		        return size;
		    }

		public boolean isEmpty() {
			return size == 0;
		}

		public boolean contains(T value) {
			for (int i = 0; i < size; i++) {
				if (table[i].equals(value)) {
					return true;
				}
			}

			return false;
		}

		public T remove(int index) {
			T value = get(index);

			for (int i = index; i < size; i++) {
				table[i] = table[i + 1];
			}

			size--;

			return value;
		}

		public String toString() {
			String value = "[";
			for (int i = 0; i < size(); i++) {
				value += get(i);
				if (i + 1 < size) {
					value += ", ";
				}
			}
			value += "]";

			return value;
		}
	}
}
