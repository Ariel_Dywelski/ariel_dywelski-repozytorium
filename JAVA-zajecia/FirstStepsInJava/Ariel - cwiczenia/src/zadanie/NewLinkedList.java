package zadanie;

public class NewLinkedList {

	private MyNode first;
	private int size = 0;

	public NewLinkedList() {
		first = null;
	}

	public void add(String element) {
		if (first == null) {
			first = new MyNode(element);
		} else {
			MyNode tmp = first;
			while (tmp.hasNext()) {
				tmp = tmp.next;
			}
			tmp.next = new MyNode(element);
		}
		size++;
	}

	public String remove(int index) {
        if(index == 0){
            String value = first.value;
            first = first.next;

            return value;
        }

        MyNode prevElement = first;
        MyNode element = first.next;
        int i = 1;
        while (element != null) {
            if(i == index){
                String value = element.value;
                prevElement.next = element.next;

                return value;
            }

            i++;
            prevElement = element;
            element = element.next;
        }

        throw new ArrayIndexOutOfBoundsException();
    }

	public int size() {
		return size;

	}

	@Override
	public String toString() {
		if (first == null) {
			return "[]";
		}

		return first.toString();
	}
}