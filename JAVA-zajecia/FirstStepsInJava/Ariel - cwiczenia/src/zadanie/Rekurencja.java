package zadanie;

public class Rekurencja {

	public static void main(String[] args) {

		print(5);
		System.out.println("Suma dla 8 jest: " + sum(8));
		System.out.println("Suma 5*4*3*2*1 = " + loop(5));
		System.out.println("Silnia od 5 rekurencyjnie:  " + rec(5));
	}

	static void print(int i) {
		System.out.println(i);
		if (i == 0) {
			return;
		}

		print(i - 1);

	}

	static int sum(int i) {
		System.out.println("Licze dla i= " + i);
		if (i == 0) {

			return i;

		}

		return i + sum(i - 1);
	}

	private static int loop(int n) {

		int result = 1;
		for (int i = 1; i <= n; i++) {
			result = result * i;
		}
		return result;
	}

	private static int rec(int n) {
		System.out.println("Licze dla n= " + n);
		if (n == 1) {
			return 1;
		}
		return n * rec(n - 1);

	}

}
