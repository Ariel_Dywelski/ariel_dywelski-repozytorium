package zadanie;

import java.util.HashSet;

public class zadanie2 {

	public static void main(String[] args) {
		Osoba person = new Osoba("Ariel", "Dywelski", 321);
		Osoba person1 = new Osoba("Ariel", "Dywelski", 32);
		Osoba person2 = new Osoba("Ela", "Nguyen Chi", 12);
		
		System.out.println(person.equals(person1));

		System.out.println(person.hashCode());
		System.out.println(person1.hashCode());
		System.out.println(person2.hashCode());
		
		HashSet<Osoba> osoba = new HashSet<>();
		osoba.add(person);
		osoba.add(person1);
		
		System.out.println(osoba);
	}

	
	
}
