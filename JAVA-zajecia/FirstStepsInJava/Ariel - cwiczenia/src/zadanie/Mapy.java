package zadanie;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Mapy {

	public static void main(String[] args) {

		HashMap<String, Integer> vehicle = new HashMap<>();

		vehicle.put("Skoda Octavia", 4);
		vehicle.put("Ford Focus", 6);
		vehicle.put("Toyota Auris", 3);

		System.out.println(vehicle);

		for (String i : vehicle.keySet()) {

			System.out.println(vehicle.get(i));
		}

		System.out.println(vehicle);

		vehicle.remove("Toyota Auris");
		System.out.println("Now my cars looks that:  " + vehicle);

		System.out.println("I do not remember whether we have such cars: " + vehicle.containsKey("Skoda Octavia"));

		vehicle.clear();
		System.out.println(vehicle);
	}

}
