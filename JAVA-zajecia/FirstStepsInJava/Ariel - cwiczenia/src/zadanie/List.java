package zadanie;

import java.util.ArrayList;

public class List {

	public static String t = "Marcin";

	public static void main(String[] args) {

		ArrayList<String> myList = new ArrayList<>();
		System.out.println(myList);

		myList.add("Ariel");
		myList.add("Ariel");
		myList.add(t);
		myList.add("Ariel");
		myList.add("Ariel");
		myList.add("Ariel");
		myList.add("Maciek");
		
		System.out.println(myList);

		for (int i = 0; i < myList.size(); i++) {

			System.out.println(myList.get(i));

		}

		myList.remove(0);

		System.out.println(myList);


		
		System.out.println(myList.indexOf("Ariel"));
		System.out.println(myList.lastIndexOf("Ariel"));
		
		System.out.println(myList.contains("Maciek"));
		
		myList.removeAll(myList);
		System.out.println(myList);
	}

}
