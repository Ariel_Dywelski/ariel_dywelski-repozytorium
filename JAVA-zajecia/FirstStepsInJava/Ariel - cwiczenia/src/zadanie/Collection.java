package zadanie;

import java.util.HashSet;

public class Collection {

	public static void main(String[] args) {

		HashSet<String> myCollect = new HashSet<>();

		myCollect.add("Skoda");
		myCollect.add("Ford");
		myCollect.add("Fiat");
		myCollect.add("Ferrari");
		myCollect.add("Maseratti");
		myCollect.add("Alfa Romeo");

		System.out.println(myCollect);

		for (String i : myCollect) {
			if (i.startsWith("F"))
				System.out.println("Print all elements of My collection:  " + i);

		}

		myCollect.remove("Fiat");
		System.out.println(myCollect);
		
		System.out.println(myCollect.contains("Fiat"));
		
		myCollect.removeAll(myCollect);
		System.out.println(myCollect);
		
	}

}
