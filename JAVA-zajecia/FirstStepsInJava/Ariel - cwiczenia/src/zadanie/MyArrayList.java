package zadanie;

import java.util.Arrays;

public class MyArrayList {

	private String[] tablica;
	private int pointer;
	private Integer[] index;

	public MyArrayList() {

		this.tablica = new String[5];
		this.pointer = 0;

	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("Tablica =[");
		for (int i = 0; i < pointer; i++) {
			sb.append(tablica[i]);

			if (i != pointer - 1) {
				sb.append(", ");
			}

		}
		sb.append("]");
		sb.append(" pointer=" + this.pointer);
		return sb.toString();

	}

	public void addToArray(String string) {
		tablica[pointer] = string;
		pointer++;
	}

	public void removeFromList(int string) {
		tablica[string] = null;
		for (int i = string; i < pointer - 1; i++) {

			tablica[i] = tablica[i + 1];

		}
		pointer--;

	}

	public int findIndexInMyArray(String indexOf) {
		int index = -1;
		for (int i = 0; i < pointer; i++) {
			if (indexOf == tablica[i]) {
				index = i;

				break;
			}

		}

		return index;

	}

}
