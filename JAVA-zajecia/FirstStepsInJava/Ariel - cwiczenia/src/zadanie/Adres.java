package zadanie;

public class Adres {

	private String ulica;
	private String miasto;
	private int numerDomu;
	
	public Adres(String ulica, String miasto, int numerDomu) {

		this.ulica = ulica;
		this.miasto = miasto;
		this.numerDomu = numerDomu;
	}

	@Override
	public String toString() {
		return "Adres ulica: " + ulica + ", miasto: " + miasto + " numer Domu " +numerDomu ;
	}

}
