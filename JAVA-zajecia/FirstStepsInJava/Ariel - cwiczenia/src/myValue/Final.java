package myValue;

public class Final {

	class A {

		final String a = "Ariel";

		final int get() {
			return 123;
		}

		public A(final MyValue a) {
			a.value = "3";

		}
	}

	class B extends A {

		public B(MyValue a) {
			super(a);

		}

		int get1() {
			return get();

		}
	}
}
