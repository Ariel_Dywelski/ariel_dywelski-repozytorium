package dataBase;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class PersonDataBase {

	public static void main(String[] args) throws IOException {

		File file = new File("C:\\Users\\Uzytkownik\\Desktop\\SDA_CWIICZENIA\\CwiczeniePliki\\PersonBase.txt");
		List<Person> persons = new ArrayList<Person>();

		persons.add(new Person("1", "Ariel", "Dywelski",1));
		persons.add(new Person("2", "Daniel", "Dywelski"));
		persons.add(new Person("3", "Adam", "Dywelski"));
		persons.add(new Person("4", "Piotr","Dywelski"));
		persons.add(new Person("5", "Jacek", "Klaman"));
		savePerson(persons);
	}

	private static String convertToString(Person person) {
		return person.getID()+ ";"+ person.getName() + ";" + person.getSurname() + ";" + "\n";
	}

	private static void savePerson (List<Person> persons) throws IOException{
		Path path = (Paths.get("C:\\Users\\Uzytkownik\\Desktop\\SDA_CWIICZENIA\\CwiczeniePliki\\PersonBase.txt"));
		try (BufferedWriter bufferedWriter = Files
				.newBufferedWriter(path)){
			persons.forEach((person -> { try{
						bufferedWriter.write(convertToString(person));
					} catch (IOException e) {
						e.printStackTrace();
					}
		}));
		}
	System.out.println("Done");
	}
	

	
}
