package dataBase;

public class Person {

	private final String ID;
	private final String name;
	private final String surname;
	private final Address adres;
	public Person(String id, String name, String surname, Address adres) {
		this.ID = id;
		this.name = name;
		this.surname = surname;
		this.adres = adres;
	}

	public Address getAdres() {
		return adres;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getID() {
		return ID;
	}

	@Override
	public String toString() {
		return "Person [ID: " + ID + ", IMI�: " + name + ", NAZWISKO: " + surname + ", ADRES: "+adres+"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((adres == null) ? 0 : adres.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (adres == null) {
			if (other.adres != null)
				return false;
		} else if (!adres.equals(other.adres))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}




	
}
