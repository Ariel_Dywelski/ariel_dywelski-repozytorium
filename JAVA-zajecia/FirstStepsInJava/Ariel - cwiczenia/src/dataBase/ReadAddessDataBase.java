package dataBase;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReadAddessDataBase {

	public static void main(String[] args) throws IOException {

		readAddressFromFile();

	}


	private static void readAddressFromFile() throws IOException {
		Path path = (Paths.get("C:\\Users\\Uzytkownik\\Desktop\\SDA_CWIICZENIA\\CwiczeniePliki\\AddressBase.txt"));
		List<Address> adresy = new ArrayList<>();
		try {
			List<String> address = Files.readAllLines(path);

			for (int i = 0; i < address.size(); i++) {

				String[] split = address.get(i).split(";");
				adresy.add(new Address(split[0], split[1]));

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		adresy.forEach(System.out::println);

	}

}

