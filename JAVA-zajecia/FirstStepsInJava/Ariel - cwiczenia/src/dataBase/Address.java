package dataBase;

public class Address {
	private final String ID;
	private final String city;

	public Address(String id, String city) {
		this.ID = id;
		this.city = city;
	}


	@Override
	public String toString() {
		return "Address [ID: " + ID + ", Miasto: " + city + "]";
	}


	public String getID() {
		return ID;
	}


	public String getCity() {
		return city;
	}


}
