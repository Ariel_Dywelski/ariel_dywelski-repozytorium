package dataBase;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AddressDataBase {

	public static void main(String[] args) throws IOException {

		File file = new File("C:\\Users\\Uzytkownik\\Desktop\\SDA_CWIICZENIA\\CwiczeniePliki\\AddressBase.txt");
		List<Address> adres = new ArrayList<Address>();

		adres.add(new Address("1", "Gdansk"));
		adres.add(new Address("2", "Warszawa"));
		adres.add(new Address("3", "Kaliska"));
		adres.add(new Address("4", "Starogard"));
		adres.add(new Address("5", "Wieck"));

		savePerson(adres);
	}

	private static String convertToString(Address adres) {
		return adres.getID() + ";" + adres.getCity() + ";" + "\n";
	}

	private static void savePerson(List<Address> adres) throws IOException {
		Path path = (Paths.get("C:\\Users\\Uzytkownik\\Desktop\\SDA_CWIICZENIA\\CwiczeniePliki\\AddressBase.txt"));
		try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path)) {
			adres.forEach((person -> {
				try {
					bufferedWriter.write(convertToString(person));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}));
		}
		System.out.println("Done");
	}
}
