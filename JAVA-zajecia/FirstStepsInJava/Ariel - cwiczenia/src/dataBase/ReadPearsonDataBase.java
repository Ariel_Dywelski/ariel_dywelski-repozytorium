package dataBase;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReadPearsonDataBase {

	public static void main(String[] args) throws IOException {

		readPersonFromFile();

	}

	private static void readPersonFromFile() throws IOException {
		Path path = (Paths.get("C:\\Users\\Uzytkownik\\Desktop\\SDA_CWIICZENIA\\CwiczeniePliki\\PersonBase.txt"));
		List<Person> osoby = new ArrayList<>();
		try {
			List<String> person = Files.readAllLines(path);

			for (int i = 0; i < person.size(); i++) {

				String[] split = person.get(i).split(";");
				osoby.add(new Person(split[0], split[1], split[2]));

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		osoby.forEach(System.out::println);

	}

}
