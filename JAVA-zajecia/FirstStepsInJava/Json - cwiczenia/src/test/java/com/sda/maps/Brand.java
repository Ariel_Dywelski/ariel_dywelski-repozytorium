package com.sda.maps;

public enum Brand {

	JEEP, MINI, MERCEDES, ALFAROMEO, LEXUS, BMW, VOLVO;

	public static Brand fromString(String input){
		String cleanInput = input.replaceAll(" ", "").toUpperCase();
		Brand result = Brand.valueOf(cleanInput);
		return result;
	}
}
