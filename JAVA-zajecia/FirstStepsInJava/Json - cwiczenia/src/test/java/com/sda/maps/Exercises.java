package com.sda.maps;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class Exercises {

	@Test
	public void warmUp() {

	        /*
	         * Deklarujemy nowa mape, W ktorej kluczem jest String, a wartoscia
	         * Integer. Np. {Piotrek=24}
	         */
	        Map<String, Integer> sampleMap = new HashMap<String, Integer>();
	        // Do mapy dodaj 2 elementy
	        sampleMap.put("Piotrek", 24);
	        sampleMap.put("Ariel", 24);

	        Assert.assertEquals(2, sampleMap.size());
	        // Usun element z mapy

	        sampleMap.remove("Ariel");

	        Assert.assertEquals(1, sampleMap.size());
	        final Integer oldValue = sampleMap.values().iterator().next();

	        sampleMap.put("Piotrek", 25);
	        final Integer newValue = sampleMap.values().iterator().next();
	        Assert.assertNotEquals(oldValue, newValue);
	        System.out.println(newValue);
	}
	/**
	 * Spojrz na String rawCars. Zawiera on lise marek samochodow wymienionych
	 * po przecinku. Twoim zadaniem jest stworzyc taka mape typu Map<String,
	 * Integer> w ktorej kluczem bedzie nazwa marki samochodu (np. mini) a
	 * wartoscia liczba wystapien danej marki w tekscie.
	 * 
	 * Przydatne metody: - {@link String#split(String)}: Metoda zmiennych typu
	 * String, dzielaca String na tablice typu String[]. Jako parametr przyjmuje
	 * String na bazie ktorego ma sie odbyc podzial. Np. "mama,tata".split(",")
	 * zwróci nami ["mama", "tata"] - {@link Map#getOrDefault(Object, Object)}:
	 * Metoda ta sluzy do zapobiegniecia zwracania nulla, kiedy dany klucz nie
	 * istnieje w mapie. Np. posiadajac mape przykladMapy = {"Piotr"=24}, gdy
	 * uzyjemy metody przykladyMapy.getOrDefault("Lukasz", 0) zamiast nulla
	 * (gdyz "Lukasz" nie znajduje sie w mapie) otrzymamy wartosc 0. tip: Jesli
	 * w mapie nie jeszcze samochodu danej marki, to znaczy, ze ile razy
	 * wystąpił do tej pory?
	 * 
	 */


	@Test
    public void letterOccurences() {
        String rawCars = "mercedes,lexus,alfa romeo,mercedes,"
                + "lexus,alfa romeo, jeep,mercedes,volvo,volvo,volvo,volvo" /*
                                                                             * "Surowy"
                                                                             * tekst,
                                                                             * ktory
                                                                             * nalezy
                                                                             * przetworzyc
                                                                             */
                + ",mini,bmw,bmw";
        String[] rawCarsArray = rawCars.split(",");
        Map<String, Integer> carToOccurrencesMap = new HashMap<String, Integer>();

        for (String car : rawCarsArray) {
            String cleanCar = car.trim();
            int value = carToOccurrencesMap.getOrDefault(cleanCar, 0);
            value++;
            carToOccurrencesMap.put(cleanCar, value);
        }
        System.out.println(carToOccurrencesMap);

        Assert.assertThat(carToOccurrencesMap, CoreMatchers.is(this.carOccurencesMap()));
    }

    @Test
    public void enumMap() {
        String rawCars = "mercedes,lexus,alfa romeo,mercedes,"
                + "lexus,alfa romeo, jeep,mercedes,volvo,volvo,volvo,volvo" + ",mini,bmw,bmw";
        String[] rawCarsArray = rawCars.split(",");
        Map<Brand, Integer> carOccurances = new EnumMap<Brand, Integer>(Brand.class);
        System.out.println(carOccurances);

        for (String car : rawCarsArray) {
            String cleanCar = car.trim();
            Brand brandCar = Brand.fromString(cleanCar);
            int value = carOccurances.getOrDefault(brandCar, 0);
            value++;
            carOccurances.put(brandCar, value);
        }
        System.out.println(carOccurances);
    }

    private Map<String, Integer> carOccurencesMap() {
        Map<String, Integer> occurrences = new HashMap<String, Integer>();
        occurrences.put("jeep", 1);
        occurrences.put("mini", 1);
        occurrences.put("mercedes", 3);
        occurrences.put("alfa romeo", 2);
        occurrences.put("lexus", 2);
        occurrences.put("bmw", 2);
        occurrences.put("volvo", 4);
        return occurrences;
    }

}