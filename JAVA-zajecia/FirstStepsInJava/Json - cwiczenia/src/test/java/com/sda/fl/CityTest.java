package com.sda.fl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;

import org.hamcrest.Matcher;
import org.junit.Test;

import com.sda.fl.csv.CsvTransformer;
import com.sda.fl.model.City;
import com.sda.fl.model.CityDeserialized;
import com.sda.fl.model.Serializer;

public class CityTest extends ForgottenLandProject {

	@Test
	public void shouldDeserialize() {

		Serializer cs = new CityDeserialized("/testland.json");
		
		Collection<City> deserializedCities = cs.deserialize();
		List<City> expectedCities = new ArrayList<City>();
		expectedCities.add(new City("Polkowice", "Dolny Slask", 25000, "Gornik"));
		expectedCities.add(new City("Krakow", "Malopolska", 1000000, "Centus"));
		expectedCities.add(new City("Gdansk", "Pomorze", 450000, "Beton"));

		assertArrayEquals(expectedCities.toArray(), deserializedCities.toArray());

	}

	@Test
	public void deserializedIfEmpty() {
		Serializer cs = new CityDeserialized("/testlandempty.json");
	
		Collection<City> deserializedCities =cs.deserialize();
		List<City> expectedCities = new ArrayList<City>();

		assertArrayEquals(expectedCities.toArray(), deserializedCities.toArray());
	}

	@Test
	public void shouldWriteToCsv() {
		City city = new City("Polkowice", "Dolny Slask", 25000, "Gornik");
		CsvTransformer csvTransformer = new CsvTransformer();
		String expected = "Polkowice,Dolny Slask,25000,Gornik";
		assertThat(csvTransformer.toCsv(city), is(expected));

	}

	@Test
	public void shouldWriteCollectionToCsv(){
		List<City> expectedCities = new ArrayList<City>();
		expectedCities.add(new City("Polkowice", "Dolny Slask", 25000, "Gornik"));
		expectedCities.add(new City("Krakow", "Malopolska", 1000000, "Centus"));
		expectedCities.add(new City("Gdansk", "Pomorze", 450000, "Beton"));
		
		
		List<String> expectedList = new ArrayList<String>();
		expectedList.add("Polkowice,Dolny Slask,25000,Gornik");
		expectedList.add("Krakow,Malopolska,1000000,Centus");
		expectedList.add("Gdansk,Pomorze,450000,Beton");
		
		CsvTransformer csvTransformer = new CsvTransformer();
		assertThat(csvTransformer.toCsv(expectedCities), is(expectedList));
		
	}
}
