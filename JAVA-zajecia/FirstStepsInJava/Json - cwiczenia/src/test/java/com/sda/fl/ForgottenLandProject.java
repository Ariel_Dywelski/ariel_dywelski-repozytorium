package com.sda.fl;

/**
 * Czas na zadanie!
 * 
 * Stworzymy program, ktory bedzie robil nastepujace rzeczy:
 *  * Deserializowal obiekty z pliku forgottenland.json
 *  * transformowal plik JSON na CSV
 *  * Tworzył hierarhiczną strukturę { region -> { name -> land } }
 *  * Posiadał testy weryfikujace swoje działanie.
 *
 * Spojrz na plik forgottenland.json. Jest to lista encji,
 * ktora musisz stworzyc analogicznie do klasy Dog.
 * Plik CSV to plik gdzie wartości oddzielone są przecinkami (zazwyczaj).
 *  * https://en.wikipedia.org/wiki/Comma-separated_values
 *  * konwerter online: https://konklone.io/json/
 *  Zadanie ekstra - zapis wyniku do pliku.
 *  Hierarchiczna struktura powinna miec postac Mapy, gdzie kluczem jest
 *  region, a wartoscia mapa, w ktorej kluczem jest nazwa krainy a wartoscia obiekt krainy,
 *  tj Map<Region, Map<NazwaKrainy, Kraina>>.
 *  
 *  Rozwiazanie powinno znajdowac sie w paczce com.sda.fl
 */
public class ForgottenLandProject {

}
