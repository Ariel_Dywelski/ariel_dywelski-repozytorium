package com.sda.dogs;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.Test;

public class DogsExtractorTest {

    @Test
    public void singeDog() {
        String dogString = DogSerializer.getResourceAsString("/dog.json");
        Dog deserializedGacek = DogSerializer.deserializeDog(dogString);
        Dog dog = new Dog("Gacek");
        assertThat(deserializedGacek, is(dog));

        String serializedGacek = DogSerializer.serializeDog(dog);
        assertThat(serializedGacek, is("{\"name\":\"Gacek\"}"));
    }

    @Test
    public void dogPack() {
        String dogsString = DogSerializer.getResourceAsString("/dogs.json");
        Collection<Dog> deserializedDogs = DogSerializer.deserializeDogs(dogsString);
        List<Dog> dogs = new ArrayList<Dog>();
        dogs.add(new Dog("Gacek"));
        dogs.add(new Dog("Azor"));
        dogs.add(new Dog("Kajtek"));
        dogs.add(new Dog("Burek"));
        assertArrayEquals(dogs.toArray(), deserializedDogs.toArray());
        
        /*
         *  Zadanie - napisz asercję sprawdzającą serializacja List<Dog> na Stringową
         *  reprezentacją zgodną z notacją JSON.
         */
    }

}
