package com.sda.maps;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import org.junit.Before;
import org.junit.Test;

public class BasicOperations {

    // Map types
    private Map<Integer, String> hashMap = new HashMap<>();

    private Map<Integer, String> treeMap = new TreeMap<>();

    private Map<Integer, String> lhMap = new LinkedHashMap<>();

    @Before
    public void clear() {
        hashMap.clear();
        treeMap.clear();
        lhMap.clear();
    }

    @Test
    public void adding() {
        this.addToMaps();
        System.out.println(hashMap);
        System.out.println(treeMap);
        System.out.println(lhMap);
    }

    @Test
    public void retrieving() {
        this.addToMaps();
        String adam = hashMap.get(10);
        System.out.println(adam);
        String someone = hashMap.get(100);
        System.out.println(someone);

        String defaultSomeone = hashMap.getOrDefault(100, "someone");
        System.out.println(defaultSomeone);
    }

    private void addToMaps() {
        hashMap.put(10, "Adam");
        hashMap.put(2, "Zosia");
        hashMap.put(15, "Basia");
        hashMap.put(10, "ZLY ADAM");

        treeMap.put(10, "Adam");
        treeMap.put(2, "Zosia");
        treeMap.put(15, "Basia");
        treeMap.put(10, "ZLY ADAM");

        lhMap.put(10, "Adam");
        lhMap.put(2, "Zosia");
        lhMap.put(15, "Basia");
        lhMap.put(10, "ZLY ADAM");
    }

}
