package com.sda.fl.model;



public class City {

	
	public City(String name, String region, int populationCount, String dominatingRace) {
		this.name = name;
		this.region = region;
		this.populationCount = populationCount;
		this.dominatingRace = dominatingRace;
	}

	private String name;
	private String region;
	private int populationCount;
	private String dominatingRace;

	public String getName() {
		return name;
	}

	public String getRegion() {
		return region;
	}

	public int getPopulation() {
		return populationCount;
	}

	public String getDominatingRace() {
		return dominatingRace;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setPopulation(int population) {
		this.populationCount = population;
	}

	public void setDominatingRace(String dominatingRace) {
		this.dominatingRace = dominatingRace;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dominatingRace == null) ? 0 : dominatingRace.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + populationCount;
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (dominatingRace == null) {
			if (other.dominatingRace != null)
				return false;
		} else if (!dominatingRace.equals(other.dominatingRace))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (populationCount != other.populationCount)
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		return true;
	}

}
