package com.sda.fll;

import com.sda.fl.csv.CsvTransformer;
import com.sda.fl.model.Serializer;

public class FantasyLandApplication {

	private final Serializer serializer;
	private final CsvTransformer transformer;
	
	public FantasyLandApplication(Serializer serializer,
			CsvTransformer transformer) {
	
		this.serializer = serializer;
		this.transformer = transformer;
	}
	
	
}
