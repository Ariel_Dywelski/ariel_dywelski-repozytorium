package com.sda.dogs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Scanner;

public class DogSerializer {

    private static final Gson gson = new Gson();

    public static String getResourceAsString(String path) {
        InputStream inputStream = DogSerializer.class.getResourceAsStream(path);
        Scanner scanner = new Scanner(inputStream);
        scanner.useDelimiter("\\A");
        final String value = scanner.hasNext() ? scanner.next() : "";
        scanner.close();
        return value;
    }

    public static Dog deserializeDog(String s) {
        return gson.fromJson(s, Dog.class);
    }

    public static String serializeDog(Dog dog) {
        return gson.toJson(dog);
    }
    
    public static Collection<Dog> deserializeDogs(String s) {
        Type t = new TypeToken<Collection<Dog>>(){}.getType();
        return gson.fromJson(s, t);
    }

    public static String serializeDogs(Collection<Dog> dogs) {
        return gson.toJson(dogs);
    }

}
