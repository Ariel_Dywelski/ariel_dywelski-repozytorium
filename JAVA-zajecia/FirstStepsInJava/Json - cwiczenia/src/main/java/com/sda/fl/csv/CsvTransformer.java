package com.sda.fl.csv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sda.fl.model.City;

public class CsvTransformer {

	public String toCsv(City city){
		
		String line = String.format(
				"%s,%s,%s,%s",
				city.getName(),
				city.getRegion(),
				city.getPopulation(),
				city.getDominatingRace());
		
		return line;
	}
	
	public List<String> toCsv(Collection<City> cities){
		List<String> result = new ArrayList<>();
		for(City city: cities){
			result.add(this.toCsv(city));
			
		}
		return result;
		
		
	}
}
