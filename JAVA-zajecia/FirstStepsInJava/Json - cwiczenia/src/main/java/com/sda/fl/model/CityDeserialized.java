package com.sda.fl.model;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CityDeserialized implements Serializer {

	private static final Gson GSON = new Gson();
	
	private String path;
	
	public CityDeserialized(String path) {
		this.path = path;
	}
	
	
	public Collection<City> deserializeCities() {
		Type t = new TypeToken<Collection<City>>() {}.getType();
		return GSON.fromJson(this.getResourceAsString(), t);
	}

	private String getResourceAsString() {
		InputStream inputStream = CityDeserialized.class.getResourceAsStream(this.path);
		Scanner scanner = new Scanner(inputStream);
		scanner.useDelimiter("\\A");
		final String value = scanner.hasNext() ? scanner.next() : "";
		scanner.close();
		return value;
	}
}
