package com.sda.fl.model;

import java.util.Collection;

public interface Serializer <T> {

	Collection<T> deserialize();

}