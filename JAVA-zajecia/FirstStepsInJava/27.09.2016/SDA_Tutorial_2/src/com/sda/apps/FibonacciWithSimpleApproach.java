package com.sda.apps;

public class FibonacciWithSimpleApproach {
	//Zad 4
	public static void main(String[] args) {
		int[] fibArray = new int [5];
		fibArray[0] = 1;
		fibArray[1] = 1;
		
		for(int i=2; i< fibArray.length; i++){
			fibArray[i] = fibArray[i-1] + fibArray[i-2];
		}
		
		System.out.println("20th number of Fibonnaci Series is: "+fibArray[4]);
	}
}
