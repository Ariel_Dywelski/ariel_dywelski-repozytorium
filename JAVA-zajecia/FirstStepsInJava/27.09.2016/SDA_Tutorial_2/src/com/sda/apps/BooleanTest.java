package com.sda.apps;

public class BooleanTest {
	//Zad 1
	public static void main(String[] args) {
		
		int a = 150;
		int b = 10;
		
		boolean isAGreaterThanB = a > b;
		
		boolean isALowerThan100 = a < 100;
		
		System.out.println("Is A greater than B: "+isAGreaterThanB);
		
		System.out.println("Is A lower than 100: "+isALowerThan100);
		
		
		if(isAGreaterThanB){
			System.out.println("A is greater than B.");
		} else {
			System.out.println("A is not greater than B.");
		}
		
		if(isALowerThan100){
			System.out.println("A is lower than 100.");
		} else {
			System.out.println("A is not lower than 100.");
		}
	}

}
