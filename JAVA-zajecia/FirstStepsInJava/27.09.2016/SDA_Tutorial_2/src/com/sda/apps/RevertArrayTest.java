package com.sda.apps;

public class RevertArrayTest {
	//Zad 2
	public static void main(String[] args) {
		int[] baseArray = new int[]{1,2,3,4,5};
		
		int arrayLength = baseArray.length;
		
		int[] revertedArray = new int[arrayLength];
		
		for(int i=0; i < arrayLength; i++){
			revertedArray[arrayLength-1-i] = baseArray[i];
		}
		
		System.out.println("Base Array: ");
		printArray(baseArray);
		
		System.out.println("Reverted Array: ");
		printArray(revertedArray);
	}

	
	static void printArray(int[] arrayToPrint){
		for(int i=0; i < arrayToPrint.length; i++){
			System.out.print(arrayToPrint[i]+" ");
		}
		System.out.println();
	}
}
