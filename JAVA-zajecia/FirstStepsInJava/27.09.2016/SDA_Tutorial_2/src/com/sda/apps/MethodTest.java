package com.sda.apps;

public class MethodTest {
	//Zad 6,7
	public static void main(String[] args) {
		int x = 10;
		int y = 20;
		increaseByFiveAndPrint(x);
		
		y = multiplyByTen(y);
		
		System.out.println(y);
	}

	static void increaseByFiveAndPrint(int a){
		System.out.println(a+5);
	}
	
	static int multiplyByTen(int b){
		return b*10;
	}
}
