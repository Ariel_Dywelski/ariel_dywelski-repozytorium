package com.sda.apps;

public class ForLoopTest {
	private static int i;

	//Zad 3
	public static void main(String[] args) {
		
		int[] baseArray = new int[]{1,2,3,4,5,6,7,8,9,10};
		
		System.out.println("Base State: ");
		printArray(baseArray);
		
		for(int i=0; i < baseArray.length; i+=2){
			baseArray[i]+=3;
		}
		
		System.out.println("Final State: ");
		printArray(baseArray);
	}
	
	static void printArray(int[] arrayToPrint){
		for(int i=0; i < arrayToPrint.length; i++){
			System.out.print(arrayToPrint[i]+" ");
		}
		System.out.println();
	}

}
