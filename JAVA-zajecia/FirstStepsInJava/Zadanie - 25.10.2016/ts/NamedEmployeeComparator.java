package com.sda.ts;

import java.util.Comparator;

public class NamedEmployeeComparator implements Comparator<NamedEmployee> {

	@Override
	public int compare(NamedEmployee emp1, NamedEmployee emp2) {

		int result = emp1.getName().compareTo(emp2.getName());
		if (result == 0) {
			result = emp1.getNumber() - emp2.getNumber();
		}
		return result;
	}

}
