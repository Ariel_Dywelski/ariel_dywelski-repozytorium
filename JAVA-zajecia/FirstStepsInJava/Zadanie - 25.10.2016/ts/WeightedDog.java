package com.sda.ts;

public class WeightedDog extends BasicDog implements Comparable<WeightedDog> {

	private double weight;

	public WeightedDog(double weight, String name) {
		super(name);
		this.weight = weight;
	}

	public double getWeight() {
		return this.weight;
	}

	@Override
	public int compareTo(WeightedDog o) {
		int result = this.getName().compareTo(o.getName());
		if (result == 0) {
			result = Double.valueOf(this.weight).compareTo(o.weight);
		}
		return result;
	}

	@Override
	public String toString() {
		return "WeightedDog [weight=" + this.weight + ", getName()=" + this.getName() + "]";
	}

}
