package com.sda;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.junit.Test;

public class BasicSetOperations {

    @Test
    public void adding() {
        Set<String> set = new HashSet<String>();
        System.out.println(set);

        // Do setu mozemy dodawac elementy
        set.add("jeden");
        set.add("dwa");
        System.out.println(set);

        // Nie mozemy dodac takiego samego elementu drugi raz.
        set.add("jeden");
        System.out.println(set);

        // Możemy dodac rowniez inna kolekcje
        List<String> toBeAdded = new ArrayList<String>();
        toBeAdded.add("trzy");
        toBeAdded.add("cztery");
        toBeAdded.add("piec");
        toBeAdded.add("piec");
        set.addAll(toBeAdded);
        System.out.println(set);
    }

    @Test
    public void removing() {
        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < 10; i++)
            set.add(i);
        System.out.println(set);

        // Mozemy usuwac elementy
        set.remove(5);

        // Mozemy usuwac kilka elementow
        Set<Integer> toBeRemoved = new HashSet<Integer>();
        toBeRemoved.add(1);
        toBeRemoved.add(2);
        toBeRemoved.add(3);
        set.removeAll(toBeRemoved);
        System.out.println(set);

        // Mozemy tez uzyc polecenie retained tak, by
        // pozostawic tylko elementy przekazane w drugiej lisice.
        Set<Integer> toBeRetained = new HashSet<Integer>();
        toBeRetained.add(8);
        toBeRetained.add(9);
        toBeRetained.add(6);
        set.retainAll(toBeRetained);
        System.out.println(set);
    }

    @Test
    public void checkingAndGetting() {
        Set<Integer> set = new HashSet<Integer>();
        set.add(1);
        set.add(2);
        System.out.println(set.contains(1));
        System.out.println(set.contains(123));

        for (Integer i : set) {
            System.out.println(i);
        }

        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
