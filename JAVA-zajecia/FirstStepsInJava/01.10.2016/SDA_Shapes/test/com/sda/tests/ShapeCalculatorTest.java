package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.shapes.Circle;
import com.sda.shapes.Rectangle;
import com.sda.shapes.Square;
import com.sda.utils.ShapeCalculator;

public class ShapeCalculatorTest {
	
	@Test
	public void testCalculateSquareArea(){
		assertTrue(ShapeCalculator.calculateArea(new Square(5)) == 25);
	}
	
	@Test
	public void testCalculateRectangleArea(){
		assertTrue(ShapeCalculator.calculateArea(new Rectangle(4,5)) == 20);
	}
	
	@Test
	public void testCalculateCircleArea(){
		assertTrue(ShapeCalculator.calculateArea(new Circle(10)) == 314.1592653589793);
	}
}
