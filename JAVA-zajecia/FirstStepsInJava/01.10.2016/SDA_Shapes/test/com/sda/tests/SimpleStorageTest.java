package com.sda.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.data.structures.SimpleStorage;
import com.sda.shapes.Circle;
import com.sda.shapes.Rectangle;
import com.sda.shapes.Shape;
import com.sda.shapes.Square;
import com.sda.utils.ShapesCountHolder;

public class SimpleStorageTest {

	@Test
	public void testAddingToStorage(){
		SimpleStorage mySimpleStorage = new SimpleStorage();
		
		Shape rectangle = new Rectangle(4,5);
		assertTrue(mySimpleStorage.isEmpty());
		
		mySimpleStorage.addShapeToList(rectangle);
		
		assertFalse(mySimpleStorage.isEmpty());
		
		mySimpleStorage.printEntries();
	}
	
	@Test
	public void testDeletingFromStorage(){
		SimpleStorage mySimpleStorage = new SimpleStorage();
		
		Shape rectangle = new Rectangle(4,5);
		mySimpleStorage.addShapeToList(rectangle);
		assertFalse(mySimpleStorage.isEmpty());
		
		
		mySimpleStorage.deleteShapeFromList(20);
		assertTrue(mySimpleStorage.isEmpty());
	}
	
	@Test
	public void testCountingRectangles(){
		SimpleStorage mySimpleStorage = new SimpleStorage();

		mySimpleStorage.addShapeToList(new Rectangle(4,5));
		mySimpleStorage.addShapeToList(new Rectangle(3,2));
		mySimpleStorage.addShapeToList(new Rectangle(1,8));
		mySimpleStorage.addShapeToList(new Rectangle(2,2));
		mySimpleStorage.addShapeToList(new Circle(2));
		mySimpleStorage.addShapeToList(new Square(8));
		
		assertTrue(mySimpleStorage.countRectangleTypes() == 5);
	}
	
	@Test
	public void testCountingShapes(){
		SimpleStorage mySimpleStorage = new SimpleStorage();

		mySimpleStorage.addShapeToList(new Rectangle(4,5));
		mySimpleStorage.addShapeToList(new Rectangle(3,2));
		mySimpleStorage.addShapeToList(new Rectangle(1,8));
		mySimpleStorage.addShapeToList(new Rectangle(2,2));
		mySimpleStorage.addShapeToList(new Circle(2));
		mySimpleStorage.addShapeToList(new Square(8));
		
		ShapesCountHolder sch = mySimpleStorage.countShapeTypesOccurrences();
		
		assertTrue(sch.getRectangles() == 4);
		assertTrue(sch.getSquares() == 1);
		assertTrue(sch.getCircles() == 1);
	}
}
