package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.shapes.Rectangle;

public class GenericTest {
	
	@Test
	public void testMyFirstFunctionality(){
		System.out.println("My first test!");
		Rectangle a = new Rectangle(4,3);
		
		System.out.println(a.area());
		
		assertTrue("Area is not zero!", a.area() == 1);
		
	}
	
	@Test
	public void testMySecondFunctionality(){
		System.out.println("My second test!");
	}
}
