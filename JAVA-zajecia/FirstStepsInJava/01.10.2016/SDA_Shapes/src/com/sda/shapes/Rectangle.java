package com.sda.shapes;

public class Rectangle implements Shape{
	private int sideA;
	private int sideB;
	
	public Rectangle(int sideA, int sideB){
		this.sideA = sideA;
		this.sideB = sideB;
	}
	
	@Override
	public double area() {
		return sideA * sideB;
	}

	@Override
	public double perimeter() {
		return 2*sideA + 2*sideB;
	}

}
