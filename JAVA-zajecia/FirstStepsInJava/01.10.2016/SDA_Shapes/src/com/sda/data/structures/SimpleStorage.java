package com.sda.data.structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sda.shapes.Circle;
import com.sda.shapes.Rectangle;
import com.sda.shapes.Shape;
import com.sda.shapes.Square;
import com.sda.utils.ShapesCountHolder;

public class SimpleStorage implements AbstractStorage {
	private List<Shape> shapesList;
	
	
	public SimpleStorage(){
		shapesList = new ArrayList<Shape>();
	}
	
	
	@Override
	public void addShapeToList(Shape shape) {
		shapesList.add(shape);
	}

	@Override
	public void deleteShapeFromList(double area) {
		Iterator<Shape> iter = shapesList.iterator();
		
		while(iter.hasNext()){
			Shape tmp = iter.next();
			
			if(tmp.area() == area){
				iter.remove();
			}
		}
		
	}

	@Override
	public boolean isEmpty() {
		return shapesList.isEmpty();
	}

	@Override
	public void printEntries() {
		for(Shape sh: shapesList){
			System.out.println(sh.getClass().getSimpleName()+" "+sh.area()+" "+sh.perimeter());
		}
	}

	@Override
	public int countRectangleTypes() {
		int counter = 0;
		for(Shape sh: shapesList){
			if(sh instanceof Rectangle){
				counter++;
			}
		}
		return counter;
	}
	
	public ShapesCountHolder countShapeTypesOccurrences(){
		ShapesCountHolder sch = new ShapesCountHolder();
		
		for(Shape sh : shapesList){
			if(sh.getClass().equals(Rectangle.class)){
				sch.incrementRectanglesCount();
			}
			
			if(sh.getClass().equals(Square.class)){
				sch.incrementSquaresCount();
			}
			
			if(sh.getClass().equals(Circle.class)){
				sch.incrementCirclesCount();
			}
		}
		
		return sch;
	}

}
