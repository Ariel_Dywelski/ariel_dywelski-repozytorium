package com.sda.utils;

public class ShapesCountHolder {
	private int rectangles;
	private int squares;
	private int circles;
	
	public int getRectangles(){
		return rectangles;
	}
	
	public int getSquares(){
		return squares;
	}
	
	public int getCircles(){
		return circles;
	}
	
	public void incrementRectanglesCount(){
		rectangles++;
	}
	
	public void incrementSquaresCount(){
		squares++;
	}
	
	public void incrementCirclesCount(){
		circles++;
	}
}
