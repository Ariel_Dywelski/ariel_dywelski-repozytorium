import java.util.Arrays;

public class IntegerStack {

    private int stack [];
    private int top; 

    public IntegerStack(int SIZE) 
    {
        stack = new int [SIZE];
        top = -1;
    }

    public void push(int i) 
    {
        if (top == stack.length)
        {
            extendStack();
        }

        stack[top]= i;
        top++;
    }

    public int pop() 
    {
        top --;
        return stack[top];
    }

    public int peek()
    {
        return stack[top];
    }

    public boolean isEmpty() 
    {
        if ( top == -1);
        {
            return true;
        }
    }

    private void extendStack()
    {
        int [] copy = Arrays.copyOf(stack, stack.length);
    }
}

