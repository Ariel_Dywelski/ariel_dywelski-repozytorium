package com.sda.exercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class StudentCouncil {

    private final static Random random = new Random();

    private final List<String> students;

    private String leader;

    public StudentCouncil(List<String> students) {
        this.students = students;
    }

    public List<String> getStudents() {
        return new ArrayList<String>(this.students);
    }

    public boolean addStudent(String name) {
        return students.add(name);
    }

    public void removeStudent(String name) {
        this.students.remove(students);
    }

    public String getLeader() {
        if (this.students.isEmpty()) {
            throw new IllegalStateException("No leader!");
        }
        return this.leader;
    }

    public void electNewLeader() {
        if (this.students.isEmpty()) {
            throw new IllegalStateException("No students to choose from.");
        } else {
            leader = students.get(random.nextInt() % students.size());
        }
    }

    public List<String> getSortedStudentsList(){
    	List <String> copy = new ArrayList<>(this.students);
    	Collections.sort(copy);
    	return copy;
    }
}
