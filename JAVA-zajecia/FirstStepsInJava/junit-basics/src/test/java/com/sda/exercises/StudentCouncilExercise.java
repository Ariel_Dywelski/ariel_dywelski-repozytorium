package com.sda.exercises;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Uff, czyz nie swierzba Cie rece by samemu stworzyc testy? Spojrz na klase
 * {@code StudentCouncil} - reprezentuje ona rade uczniow. Zapozniaj sie z nia.
 * 
 * PYTAJCIE, PYTAJCIE, PYTAJCIE - prowadzacego, siebie nawzajem, wasatego wujka
 * Google i ciocie Stack Overflow.
 * 
 * Stworz testy sprawdzajace nastepujace funkcjonalnosci: - Dodawanie studenta -
 * Dodawanie 10 studentow - Usuwanie studenta - Usuniecie >1 studentow. -
 * Pobieranie lidera, kiedy nikogo nie ma - Udane pobieranie lidera - Wybor
 * nowego lidera, kiedy nikogo nie ma - Udane wybieranie nowego lidera.
 * 
 * Po tym jak uda Ci sie napisac testy, wez kilka glebokich wdechow i zastanow
 * sie co mozna poprawic. Jesli wszystko jest wspaniale, sprobuj rozwiazac
 * ponizsze zadania: - Czy mozna napisac test badajacy w jakiej znajduja sie
 * studenci? Jesli tak, stworz taki test. - Stworz metode w klasie
 * {@code StudentCouncil}, ktora zwroci nowa liste ze studentami posortowanymi
 * ALFABETYCZNIE. Oczywisice napisz do niej test. - Stworz metode, ktora bedzie
 * wybierala na lidera studenta o danym imieniu. Jesli nie ma studenta o danym
 * imieniu, metoda powinna rzucac {@code IllegalArgumentException}. Oczywisice
 * testy rowniez.
 */
public class StudentCouncilExercise {

	private StudentCouncil sc;

	@Before
	public void beforeTest() {
		List<String> studentContainer = new ArrayList<>();
		this.sc = new StudentCouncil(studentContainer);
	}

	@Test
	public void addStudent() {
		sc.addStudent("Maciek");
		List<String> retrivedStudents = sc.getStudents();
		assertThat(retrivedStudents.size(), is(1));
		assertThat(retrivedStudents.contains("Maciek"), is(true));

	}

	@Test
	public void addTenStudents() {
		List<String> expectedStudents = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			sc.addStudent(String.valueOf(i));
			expectedStudents.add(String.valueOf(i));

		}
		List<String> retrivedList = sc.getStudents();
		assertThat(retrivedList, equalTo(expectedStudents));

	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionEmptyLeader() {
		sc.getLeader();
	}

	@Test
	public void shouldMantainOrder() {
		List<String> expectedStudents = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			sc.addStudent(String.valueOf(i));
			expectedStudents.add(String.valueOf(i));

		}
		List<String> retrivedList = sc.getStudents();
		for (int n = 0; n < expectedStudents.size(); n++) {
			assertThat(retrivedList.get(n), equalTo(expectedStudents.get(n)));
			System.out.println(expectedStudents);
		}
	}

	@Test
	public void shouldSorted() {
		String asia = "Asia";
		String basia = "Basia";
		String kasia = "Kasia";
		String lukasz = "Lukasz";
		String zygmunt = "Zygmunt";
		String ariel = "Ariel";
// znaki specjalne na przyk�ad � przeskakuje na koniec sortowanej listy !!!
		sc.addStudent(zygmunt);
		sc.addStudent(lukasz);
		sc.addStudent(basia);
		sc.addStudent(kasia);
		sc.addStudent(asia);
		sc.addStudent(ariel);

		System.out.println(sc.getStudents());
		System.out.println(sc.getSortedStudentsList());

	}
}
