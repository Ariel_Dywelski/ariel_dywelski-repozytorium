package com.sda.example;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BeforeAndAfterTest {

    private static MapContainer container;

    @BeforeClass
    public static void setUp() {
        container = new MapContainer();
    }

    @Before
    public void before() {
        container.addToMap(0, "Nauczyciel");
    }

    @Test
    public void performFirstTest() {
        System.out.println("Before test 1: " + container);
        assertTrue(container.size() == 1);
        container.addToMap(1, "Piotr");
        container.addToMap(2, "Pawel");
        container.addToMap(3, "Lukasz");
        assertTrue(container.size() == 3);
    }

    @Test
    public void performSecondTest() {
        System.out.println("Before test 2: " + container);
        assertTrue(container.size() == 1);
        container.addToMap(1, "Piotr");
        container.addToMap(2, "Pawel");
        container.addToMap(3, "Lukasz");
        assertTrue(container.size() == 3);
    }

    @After
    public void after() {
        // this.container.clear();
    }

    @AfterClass
    public static void tearDown() {
        container.destroy();
        System.out.println(container);
    }

}
