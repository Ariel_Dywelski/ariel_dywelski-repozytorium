package com.sda.exercises;

/**
 * Spojrz na klase {@code SuperGreeter}. Twoim zadaniem jest napisac testy
 * sprawdzajace dzialanie tej klasy. Testy te powinny sprawdzac nastepujace
 * przypadki:
 *  * Test(a moze testy?) sprawdzajace czy faktycznie metoda wita osobe
 *    przekazana jako argument.
 *  * Test sprawdzajacy jako zachowuje sie metoda po przekazaniu funkcji null
 *  * Test sprawdzajacy jak zachowuje sie metoda po przekazaniu pustego Stringa.
 *  * Test sprawdzajacy jak zachowuje sie metoda po przekazaniu arugmentu "World".
 */
public class BasicExercise {

}
