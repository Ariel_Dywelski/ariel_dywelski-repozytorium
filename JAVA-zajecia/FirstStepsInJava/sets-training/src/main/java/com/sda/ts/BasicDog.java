package com.sda.ts;

public class BasicDog {

    private String name;

    public BasicDog(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "BasicDog [name=" + this.name + "]";
    }

}
