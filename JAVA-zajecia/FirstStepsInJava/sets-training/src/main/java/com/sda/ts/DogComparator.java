package com.sda.ts;

import java.util.Comparator;

public class DogComparator implements Comparator<BasicDog> {

    public int compare(BasicDog o1, BasicDog o2) {
        int result  = o1.getName().compareTo(o2.getName());
        return result;
    }

}
