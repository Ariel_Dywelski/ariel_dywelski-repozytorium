package com.sda.ts;

public class BasicEmployee implements Comparable<BasicEmployee> {

    private int number;

    public BasicEmployee(int number) {
        this.number = number;
    }

    public int getNumber() {
        return this.number;
    }

    public int compareTo(BasicEmployee o) {
        // Integer.valueOf(this.number).compareTo(o.number);
        int result;
        if (this.number > o.number) {
            result = 1;
        } else if (this.number < o.number) {
            result = -1;
        } else {
            result = 0;
        }
        return result;
    }

    @Override
    public String toString() {
        return "BasicEmployee [number=" + this.number + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.number;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BasicEmployee other = (BasicEmployee) obj;
        if (this.number != other.number)
            return false;
        return true;
    }

}
