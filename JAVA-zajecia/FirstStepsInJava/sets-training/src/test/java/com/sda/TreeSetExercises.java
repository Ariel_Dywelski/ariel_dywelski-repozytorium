package com.sda;

import static org.junit.Assert.assertEquals;
import com.sda.ts.BasicDog;
import com.sda.ts.BasicEmployee;
import com.sda.ts.DogComparator;
import com.sda.ts.NamedEmployee;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.junit.Test;

public class TreeSetExercises {

    private final static Random RANDOM = new Random();

    @Test
    public void example() {
        Set<BasicEmployee> treeSet = new TreeSet<BasicEmployee>();
        for (int i = 0; i < 10; i++) {
            BasicEmployee employee = new BasicEmployee(Math.abs(RANDOM
                            .nextInt()));
            treeSet.add(employee);
        }
        for (BasicEmployee e : treeSet) {
            System.out.println(e);
        }
        Map<BasicDog, Integer> map = new TreeMap<>(new DogComparator());
        map.put(new BasicDog("Wazor"), 10);
        map.put(new BasicDog("Azor"), 11);
        map.put(new BasicDog("Zyzor"), 12);
        map.put(new BasicDog("Zazor"), 13);
        System.out.println(map);
    }

    /**
     * Twoim zadaniem jest napisanie Comparator<NamedEmployee>, który będzie
     * porownywal NamedEmployee. Najpierw ma porownywac po imieniu, a nastepnie
     * po numerze pracownika, tj. {"Adam", 10}, ma musi byc przed {"Tomasz, 1}
     */
    @Test
    public void sortingWithComparator() {
        Set<NamedEmployee> employees = new TreeSet<NamedEmployee>();
        NamedEmployee adam = new NamedEmployee("Adam", 10);
        NamedEmployee tomasz = new NamedEmployee("Tomasz", 1);
        NamedEmployee zuza = new NamedEmployee("Zuza", 2);
        NamedEmployee bartosz = new NamedEmployee("Bartosz", 2);
        NamedEmployee roman = new NamedEmployee("Roman", 3);
        NamedEmployee chudyRoman = new NamedEmployee("Roman", 0);

        employees.add(adam); // 1
        employees.add(tomasz); // 5
        employees.add(zuza); // 6
        employees.add(bartosz); // 2
        employees.add(roman); // 4
        employees.add(chudyRoman); // 3

        Set<NamedEmployee> sortedEmployees = new LinkedHashSet<NamedEmployee>();
        sortedEmployees.add(adam);
        sortedEmployees.add(bartosz);
        sortedEmployees.add(chudyRoman);
        sortedEmployees.add(roman);
        sortedEmployees.add(tomasz);
        sortedEmployees.add(zuza);

        Iterator<NamedEmployee> it = employees.iterator();
        Iterator<NamedEmployee> sit = sortedEmployees.iterator();
        while (it.hasNext() && sit.hasNext()) {
            assertEquals(sit.next(), it.next());
        }

    }

    /**
     * Twoim zadaniem jest implementacja interface Comparable w klasie
     * WeightedDog, tak by pies był najpierw sortowany po najpierw po imieniu,
     * nastepnie po wadze. Druga czescia zadania jest napisanie testow
     * sprawdzajacych dzialanie Twojej implementacji.
     */
    @Test
    public void sortingWithComparable() {

    }

    /**
     * Twoim zadaniem jest napisanie metody, ktora porowna czas dodawania do Setow.
     * Musisz zadeklarowac trzy rozne rodzaje setow - HashSet, LinkedHashSet oraz 
     * TreeSet.
     * Nastepnie zmierz czas jaki zajalelo dodanie do kazdego z setu po 1000 elementow
     * Elementy te powinny byc kolejnymi pseudolosowymi liczbami uzyskanymi przez zmienną
     * RANDOM. 
     * Operacje powtorz 1000 razy dla kazdej z implementacji i zmierz sredni czas.
     * Zadbaj o to, by test byl mozliwie miarodajny
     * Do aktualnego czasu mozesz uzyskac dostep poprzez {@code System#nanoTime()} lub 
     * {@code System#currentTimeMillis()}
     */
    @Test
    public void efficencyComprasion() {
    }

}
