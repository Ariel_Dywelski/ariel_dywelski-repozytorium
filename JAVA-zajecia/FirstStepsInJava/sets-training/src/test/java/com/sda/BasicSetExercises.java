package com.sda;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.security.auth.callback.Callback;

import org.junit.Test;

public class BasicSetExercises {

	/**
	 * Twoim zadaniem jest zmiana wartości zawartej w secie, np. jeśli
	 * znajdują się w nim wartości [1, 2, 3, 4] to po zmianach powinien
	 * zawierać wartość [2, 4, 6, 8]
	 */
	@Test
	public void changingValues() {
		Set<Integer> set = new HashSet<Integer>();
		for (int i = 1; i < 11; i++) {

			set.add(i);
		}

		Set<Integer> set2 = new HashSet<Integer>();

		for (Integer e : set) {
			e = e * 2;
			set2.add(e);
		}
		set.removeAll(set);
		set.addAll(set2);

		System.out.println(set);

		Set<Integer> expectedSet = new HashSet<Integer>();
		for (int i = 1; i < 11; i = i + 1) {

			expectedSet.add(i * 2);

		}
		assertTrue(set.containsAll(expectedSet));
	}

	/**
	 * Mając mapę numberSquareMap zawierającą jako klucz liczbe od 1 do 10,
	 * a jako wartość kwadrat tej liczby oblicz (poprzez entrySet() mapy)
	 * sumę kluczy oraz wartości.
	 */
	@Test
	public void sum() {
		Map<Integer, Integer> numberSquareMap = new HashMap<Integer, Integer>();
		for (int i = 1; i < 11; i++) {
			numberSquareMap.put(i, i * i);
		}
		int calculatedSum = 0;
		int calculatedSumOfSquares = 0;
		numberSquareMap.entrySet();
		Set<Entry<Integer, Integer>> entrySet = numberSquareMap.entrySet();
		for (Entry<Integer, Integer> e : entrySet) {
			calculatedSum += e.getKey();
			calculatedSumOfSquares += e.getValue();

		}
		System.out.println(calculatedSum);
		System.out.println(calculatedSumOfSquares);
		System.out.println(numberSquareMap.entrySet());

		int expectedSum = 0;
		int expectedProduct = 0;
		for (int i = 1; i < 11; i++) {
			expectedSum = expectedSum + i;
			expectedProduct = expectedProduct + i * i;
		}
		
		System.out.println(expectedSum);
		System.out.println(expectedProduct);
		assertThat(calculatedSum, is(expectedSum));
		assertThat(calculatedSumOfSquares, is(expectedProduct));

	}

}
