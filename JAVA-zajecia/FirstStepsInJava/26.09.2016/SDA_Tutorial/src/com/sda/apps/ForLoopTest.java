package com.sda.apps;

public class ForLoopTest {
	//ZAD 8
	public static void main(String[] args) {
		int[] myNumbers = new int[]{2,3,4,5,6};
		
		for(int i=0; i<5; i++){
			System.out.println(myNumbers[i]);
		}
		
		for(int i=4; i>=0; i--){
			System.out.println(myNumbers[i]);
		}
	}
}
