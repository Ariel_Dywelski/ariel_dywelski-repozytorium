package com.sda.apps;

public class PrintNameUsingLoop {
	//ZAD 9
	public static void main(String[] args) {
		String myName = "Kasper";
		
		for(int i=0; i < 10; i++){
			System.out.println(i+" "+myName);
		}
		
		int j=0;
		
		while(j<10){
			System.out.println(myName);
			j++;
		}
	}
}
