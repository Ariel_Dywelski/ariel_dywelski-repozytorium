package com.sda.apps;

import java.util.Scanner;

public class Delta {
	//ZAD 1
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		int c = scanner.nextInt();
		
		int delta = b*b - 4*a*c;
		
		if(delta > 0){
			System.out.println("TWO ZEROES");
		} else if (delta < 0){
			System.out.println("NO ZEROES");
		} else {
			System.out.println("ONE ZERO");
		}
		
		scanner.close();
	}
}
