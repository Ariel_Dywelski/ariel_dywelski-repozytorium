package com.sda.apps;

import java.util.Scanner;

public class LogicTest {
	//ZAD 3
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int a = scanner.nextInt();
		
		if( a >= 0 && a <= 10){
			System.out.println("You entered a number between 0 and 10.");
		}
		
		if(a == 10 || a == 50){
			System.out.println("OK");
		}
		
		scanner.close();
	}
}
