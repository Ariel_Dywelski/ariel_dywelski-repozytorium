package com.sda.bear;

public class TeddyBear {
	private String name;
	private String favouriteSnack;
	private int age;
	
	public TeddyBear(){
		this.name = "Teddy";
		this.favouriteSnack = "Fries";
		this.age = 1;
	}
	
	public TeddyBear(String name){
		this.name = name;
		this.favouriteSnack = "Fries";
		this.age = 1;
	}
	
	public TeddyBear(String name, String favouriteSnack){
		this.name = name;
		this.favouriteSnack = favouriteSnack;
		this.age = 1;
	}
	
	public TeddyBear(String name, String favouriteSnack, int age){
		this.name = name;
		this.favouriteSnack = favouriteSnack;
		this.age = age;
	}
	
	public String getName(){
		return name;
	}
	
	public int getAge(){
		return age;
	}
	
	public void sayHello(){
		System.out.println("My name is: "+name+" and my favourite snack is: "+favouriteSnack);
	}
	
	public void sayHello(String additionalMessage){
		System.out.println("This is: "+name+" "+additionalMessage);
	}
}
