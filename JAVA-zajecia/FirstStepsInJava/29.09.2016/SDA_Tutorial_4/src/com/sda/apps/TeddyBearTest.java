package com.sda.apps;

import com.sda.bear.TeddyBear;

public class TeddyBearTest {

	public static void main(String[] args) {
		TeddyBear firstBear = new TeddyBear();
		firstBear.sayHello();
		
		String name2 = "Leonardo";
		TeddyBear secondBear = new TeddyBear(name2);
		secondBear.sayHello();
		
		String name3 = "Michael Angelo";
		TeddyBear thirdBear = new TeddyBear(name3);
		thirdBear.sayHello();
		
		
		TeddyBear fourthBear = new TeddyBear();
		fourthBear.sayHello();
		
		secondBear.sayHello("And I am fluffy");
		secondBear.sayHello("Its great");
		//-------------------------------------
		TeddyBear firstBearToCompare = new TeddyBear("Buster", "chips", 22);
		TeddyBear secondBearToCompare = new TeddyBear("Chrystler", "potatoes", 20);
		
		compareTwoBears(firstBearToCompare, secondBearToCompare);
		
	}
	
	public static void compareTwoBears(TeddyBear a, TeddyBear b){
		if(a.getAge() > b.getAge()){
			System.out.println(a.getName()+" is older than "+b.getName());
		} else if(a.getAge() < b.getAge()){
			System.out.println(b.getName()+" is older than "+a.getName());
		} else {
			System.out.println("Equal age");
		}
	}

}
