import java.util.Scanner;

/**
 * Created by Ariel on 2017-02-01.
 */


public class Problem7_TenThousandFirstPrimeNumber {
    public static void main(String[] args) {

        calculatePrimeNumbers();

    }

    private static void calculatePrimeNumbers() {
        System.out.println("Please give prime number index which you want find: ");
        Scanner scanner = new Scanner(System.in);
        int maxNumberOfPrime = scanner.nextInt();
        int numberOfPrimes = 0;

        int number = 1;
        int limitNumberValue = 10000000;
        boolean[] primeValue = new boolean[limitNumberValue];
        for (int i = 2; i < limitNumberValue; i++) {
            if (primeValue[i]) {
                continue;
            }

            numberOfPrimes++;

            if (numberOfPrimes == maxNumberOfPrime) {
                number = i;
                break;
            }

            for (int j = i + i; j < limitNumberValue; j += i)
                primeValue[j] = true;
        }
        System.out.println("Number of primes number: " + numberOfPrimes);
        System.out.println(numberOfPrimes+" prime number is: " + number);

    }
}
