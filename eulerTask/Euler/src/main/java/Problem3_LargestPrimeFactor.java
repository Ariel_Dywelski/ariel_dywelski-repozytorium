/**
 * Created by Ariel on 2017-02-01.
 */


public class Problem3_LargestPrimeFactor {

    public static void main(String[] args) {
        calculateLargestPrimeFactor();
    }

    private static void calculateLargestPrimeFactor() {
        /**
         * The prime factors of 13195 are 5, 7, 13 and 29.
         What is the largest prime factor of the number 600851475143 ?
         */

        long givingValue = 600851475143L;
        long newNumber = givingValue;
        long largestFactor = 0;

        int counter = 2;
        while (counter * counter <= newNumber) {
            if (newNumber % counter == 0) {
                newNumber = newNumber / counter;
                largestFactor = counter;
            } else {
                counter = (counter == 2) ? 3 : counter + 2;
            }
        }
        if (newNumber > largestFactor) {
            largestFactor = newNumber;

        }

        System.out.println(largestFactor);

    }
}

