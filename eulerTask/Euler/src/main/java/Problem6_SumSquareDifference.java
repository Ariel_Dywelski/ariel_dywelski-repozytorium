/**
 * Created by Ariel on 2017-02-01.
 */


public class Problem6_SumSquareDifference {
    public static void main(String[] args) {
        calculateDifference(15);
    }

    private static void calculateDifference(int n) {
        int sumOfSquares = 0;
        int squareOfTheSum = 0;
        for (int i = 1; i <= n; i++) {
            sumOfSquares = sumOfSquares + (i * i);

            squareOfTheSum = squareOfTheSum + i;
        }

        squareOfTheSum = squareOfTheSum * squareOfTheSum;

        System.out.println("This is square of sum: " +squareOfTheSum);
        System.out.println("This is sum of square: " +sumOfSquares);

        System.out.println("This is difference between square of sum and sum of square for your value "
                +n+": "
                +(squareOfTheSum-sumOfSquares));

    }

}
