/**
 * Created by Ariel on 2017-02-01.
 */


public class Problem4_LargestPalindromeProduct {

    public static void main(String[] args) {
        calculateLargestPalindrome();
    }

    private static void calculateLargestPalindrome() {
        int palindrome = 0;
        int maxValue = 0;

        for (int i = 999; i>100; i--){
            for (int j=i; j>100; j--){
                palindrome = i*j;

                if(isPalindrome(palindrome)){
                    if(palindrome>maxValue){
                        maxValue=palindrome;
                    }
                }
            }
        }
        System.out.println(maxValue);
    }

    private static boolean isPalindrome(int palindrome) {
        boolean isPalindrome = false;
        StringBuilder builder = new StringBuilder(Integer.toString(palindrome)).reverse();
        if(builder.toString().equals(Integer.toString(palindrome))){
            isPalindrome=true;
        }
        return isPalindrome;
    }
}
