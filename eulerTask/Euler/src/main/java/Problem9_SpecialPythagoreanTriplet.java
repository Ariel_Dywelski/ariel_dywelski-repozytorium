

public class Problem9_SpecialPythagoreanTriplet {
    public static void main(String[] args) {
        calculatePythagoreanTriplet();
    }

    private static void calculatePythagoreanTriplet() {
        int sum = 1000;
        for (int a =0; a<=sum; a++){
            for (int b = a+1; b<=sum; b++){
                for (int c = b+1; c<=sum; c++){
                    if(a+b+c == sum && (c*c == a*a+b*b)){
                        System.out.println("Value of a*b*c: " +(a*b*c));
                        System.out.println("Value a: " +a);
                        System.out.println("Value b: " +b);
                        System.out.println("Value c: " +c);

                    }
                }
            }
        }
    }

}
