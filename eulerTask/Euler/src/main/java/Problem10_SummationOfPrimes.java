import java.math.BigInteger;

/**
 * Created by Ariel on 2017-02-02.
 */


public class Problem10_SummationOfPrimes {
    public static void main(String[] args) {
        calculateSumOfPrimesBelowTwoMillions();
        calculateSumOfPrimesNumberBelowTwoMillionsWithSieveOfEratosthenes();
    }

    private static void calculateSumOfPrimesBelowTwoMillions() {
        long beginOperationTime = System.currentTimeMillis();
        BigInteger sumOfPrimes = new BigInteger("2");
        int upperBound = 2000000;
        boolean isPrime = true;
        for(int i =3 ; i<upperBound; i++){
            double aa = Math.sqrt((double)i);
            for (int j = 2; j<=aa; j++){
                if(i%j == 0){
                    isPrime = false;
                    break;
                }
            }
            if (isPrime){
                sumOfPrimes = sumOfPrimes.add(BigInteger.valueOf(i));
            }
            isPrime = true;
        }
        long endOperationTime = System.currentTimeMillis();

        System.out.println("Sum of primes: " + sumOfPrimes + ". Operation time is: "+(endOperationTime-beginOperationTime)+" ms");
    }

    private static void  calculateSumOfPrimesNumberBelowTwoMillionsWithSieveOfEratosthenes(){
        long beginOperatioTime = System.currentTimeMillis();
        int upperBound = 2000000;
        boolean [] isPrime = new boolean[upperBound];
        for (int i = 2; i<upperBound; i++){
            isPrime[i] = true;
        }
        for (int i = 2; i< (int) Math.sqrt(upperBound)+1; i++){
            if (isPrime[i]){
                for(int j = 2; j*i<upperBound;j++){
                    isPrime[j*i] = false;
                }
            }
        }
        long sumOfPrimes = 0;
        for (int i = 2; i<upperBound; i++){
            if (isPrime[i]){
                sumOfPrimes = sumOfPrimes+i;
            }
        }
        long endOperationTime = System.currentTimeMillis();
        System.out.println("Sum of primes: " +sumOfPrimes + ". Operation time is: "+(endOperationTime-beginOperatioTime)+" ms");
    }
}
