/**
 * Created by Ariel on 2017-02-01.
 */


public class Problem1_AddNaturalNumber {

    public static void main(String[] args) {

        addNumber();
    }

    private static void addNumber() {

        /**
         * If we list all the natural number below 10 that are multiples of 3 or 5,
         * we get 3,5,6,9. The sum of these multiples is 23.
         *
         * Find the sum of all the multiples of 3 or 5 below 1000.
         */

        int multiplesSum = 0;
        for(int i = 0; i<1000; i++){
            if((i%3 ==0) || (i%5 == 0)){
                multiplesSum = multiplesSum +i;
            }
        }
        System.out.println(multiplesSum);
    }

}
