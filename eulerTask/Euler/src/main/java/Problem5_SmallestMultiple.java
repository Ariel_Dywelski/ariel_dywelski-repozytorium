/**
 * Created by Ariel on 2017-02-01.
 */


public class Problem5_SmallestMultiple {

    public static void main(String[] args) {
        calculateSmallestMultiple();
        calculateSM();
    }

    private static void calculateSmallestMultiple() {
        final byte P[] = new byte[] { 2, 3, 5, 7,11,13,17,19 }, N = 20;

        long s = 1, p;
        for (int i = 0; i < P.length; i++){
            p = 1;
            while (p * P[i] < N)
                p *= P[i];
            s *= p;
        }
        System.out.println(s);
    }


    private static void calculateSM(){
        long begin = System.currentTimeMillis();
        int i = 20;
        while (true)
        {
            if (
                    (i % 5 == 0) &&
                    (i % 7 == 0) &&
                    (i % 9 == 0) &&
                    (i % 11 == 0) &&
                    (i % 13 == 0) &&
                    (i % 16 == 0) &&
                    (i % 17 == 0) &&
                    (i % 19 == 0) )
            {
                break;
            }
            i += 20;
        }
        long end = System.currentTimeMillis();
        System.out.println(i);
        System.out.println(end-begin + "ms");
    }

}
